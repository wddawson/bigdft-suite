License
=======

BigDFT-suite is a free and open source collection of software packages.
Each of the different packages have their own associated license. Below are
the licenses associated with codes that are distributed with the source of
the BigDFT-suite.

* ``atlab``: GPLv3
* ``bigdft``: GPLv2
* ``bundler``: GPLv2
* ``chess``: GPLv2
* ``futile``: GPLv3
* ``GaIN``: GPLv3
* ``libABINIT``: GPLv3
* ``liborbs``: GPLv2
* ``libxc``: Mozilla Public License v2
* ``ntpoly``: MIT
* ``pseudo``: GPLv2
* ``psolver``: GPLv2
* ``PyBigDFT``: LGPLv3
* ``PyYAML``: MIT
* ``spred``: GPLv2
