Installation
============

BigDFT can be installed directly using a package manager or compiled from 
source.

Install via Docker
------------------

Docker is a virtualization platform that allows us to distribute BigDFT and
all of its associated dependencies in a single container. The BigDFT container,
along with detailed instructions, is available here_.

.. code:: bash

    sudo docker run -ti \
      -e OMP_NUM_THREADS=4 \
      -v $PWD:/results  -w /results \
      bigdft/runtime:openmpi \
      mpirun -np 2 bigdft

.. _here: https://hub.docker.com/r/bigdft/runtime//

Install via Conda
-----------------

Conda is a cross-platform package and environment management system which can
be used to install BigDFT on Linux platforms (Mac is not available due to
compiler bugs). It can be used to install BigDFT:

.. code:: bash

    conda install -c conda-forge bigdft-suite

Install via pip
---------------

pip can be used to install the client version of BigDFT. The client version
can't be used to run actual calculations, but can be used to generate input
files and perform postprocessing.

.. code:: bash

    pip install PyBigDFT

Install from Source
-------------------

.. toctree::
   :maxdepth: 1

   source-install
   minimum-install
   errors-and-fixes
