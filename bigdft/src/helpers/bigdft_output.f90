module module_bigdft_output
  use yaml_output
  use yaml_strings, only: yaml_toa, operator(+), operator(//), f_strcpy, yaml_date_and_time_toa
  use f_utils, only: f_open_file, f_file_exists, f_close
  use numerics, only: Ha_eV, Bohr_Ang, AU_GPa, Ha_THz, Ha_cmm1

  implicit none

  public
  
  integer, private :: verbose=2    !< Verbosity of the output, control the level of writing (normal by default)

contains
  
  pure function get_verbose_level()
    implicit none
    integer :: get_verbose_level
    get_verbose_level=verbose
  end function get_verbose_level

  subroutine set_verbose_level(verbosity)
    implicit none
    integer, intent(in) :: verbosity
    verbose=verbosity
  end subroutine set_verbose_level

end module module_bigdft_output
