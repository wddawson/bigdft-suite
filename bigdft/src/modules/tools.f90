module tools
  implicit none
  private

  public :: convertField, convertPositions, transformCoordinates
  public :: kernelAnalysis, extractSubMatrix, analyseCoeffs
  public :: peelMatrix, multiplyMatrices, matrixPower
  public :: suggestCutoff, minimiseBox
  public :: exportWavefunction, exportProjector
  public :: atomicWavefunctions
  public :: testGPU
  public :: memoryGuess

contains

  subroutine convertField(fileFrom, fileTo)
    use box
    use IObox
    use module_precisions
    use module_bigdft_arrays
    implicit none
    character(len = *), intent(in) :: fileFrom, fileTo

    type(cell) :: mesh
    integer :: nspin, nspin2, nat
    real(dp), dimension(:,:), allocatable :: rhocoeff
    real(gp), dimension(:,:), pointer :: rxyz
    integer, dimension(:), pointer :: iatype, nzatom

    write(*,*) "Read density file..."
    call read_field_dimensions(fileFrom, "P", mesh%ndims, nspin)
    rhocoeff = f_malloc([int(mesh%ndim),nspin], id='rhocoeff')
    call read_field(fileFrom, "P", mesh%ndims, &
         mesh%hgrids, nspin2, int(mesh%ndim), &
         nspin, rhocoeff, nat, rxyz, iatype, nzatom)
    write(*,*) "Write new density file..."

    call dump_field(fileTo, mesh, nspin, rhocoeff, &
         rxyz, iatype, nzatom, nzatom)

    call f_free(rhocoeff)

    call f_free_ptr(rxyz)
    call f_free_ptr(iatype)
    call f_free_ptr(nzatom)
    write(*,*) "Done"
  end subroutine convertField

  subroutine convertPositions(fileFrom, fileTo)
    use module_precisions
    use module_bigdft_arrays
    use module_atoms
    implicit none
    character(len = *), intent(in) :: fileFrom, fileTo

    integer :: irad
    type(atomic_structure) :: astruct
    character(len=1024) :: fcomment
    real(gp) :: energy
    real(gp), dimension(:,:), pointer :: fxyz

    call set_astruct_from_file(fileFrom, 0, astruct, fcomment, energy, fxyz)

    !find the format of the output file
    if (index(fileTo,'.xyz') > 0) then
       irad=index(fileTo,'.xyz')
       astruct%inputfile_format='xyz  '
    else if (index(fileTo,'.ascii') > 0) then
       irad=index(fileTo,'.ascii')
       astruct%inputfile_format='ascii'
    else if (index(fileTo,'.yaml') > 0) then
       irad=index(fileTo,'.yaml')
       astruct%inputfile_format='yaml '
    else
       irad = len(trim(fileTo)) + 1
    end if

    if (associated(fxyz)) then
       call astruct_dump_to_file(astruct, fileTo(1:irad-1), &
            trim(fcomment) // ' (converted from '// fileFrom //")",&
            energy, forces = fxyz)
       call f_free_ptr(fxyz)
    else
       call astruct_dump_to_file(astruct, fileTo(1:irad-1), &
            trim(fcomment) // ' (converted from '// fileFrom //")",&
            energy)
    end if
  end subroutine convertPositions

  subroutine transformCoordinates(direction, fileFrom, fileTo)
    use module_precisions
    use module_bigdft_arrays
    use module_bigdft_errors
    use module_atoms
    use internal_coordinates
    implicit none
    character(len = *), intent(in) :: direction, fileFrom, fileTo

    type(atomic_structure) :: astruct
    character(len=1024) :: fcomment
    real(gp) :: energy
    real(gp), dimension(:,:), pointer :: fxyz
    real(gp), dimension(:,:), allocatable :: rxyz_int
    integer,dimension(:), allocatable :: na, nb, nc
    integer,dimension(:,:),allocatable :: atoms_ref
    logical :: file_exists
    integer :: iat, irad

    if (direction=='carint') then
       write(*,*) 'Converting cartesian coordinates to internal coordinates.'
    else if (direction=='intcar') then
       write(*,*) 'Converting internal coordinates to cartesian coordinates.'
    else if (direction=='carcar') then
       write(*,*) 'Converting cartesian coordinates to cartesian coordinates.'
    else
       call f_err_throw("wrong switch for coordinate transforms", err_name='BIGDFT_RUNTIME_ERROR')
    end if
    call set_astruct_from_file(fileFrom, 0, astruct, fcomment, energy, fxyz)
    if (associated(fxyz)) call f_free_ptr(fxyz)

    !find the format of the output file
    if (index(fileTo,'.xyz') > 0) then
       irad=index(fileTo,'.xyz')
       astruct%inputfile_format='xyz  '
    else if (index(fileTo,'.ascii') > 0) then
       irad=index(fileTo,'.ascii')
       astruct%inputfile_format='ascii'
    else if (index(fileTo,'.int') > 0) then
       irad=index(fileTo,'.int')
       astruct%inputfile_format='int  '
    else if (index(fileTo,'.yaml') > 0) then
       irad=index(fileTo,'.yaml')
       astruct%inputfile_format='yaml '
    else
       irad = len(trim(fileTo)) + 1
    end if

    ! Check whether the output file format is correct
    if (direction=='carint') then
       ! output file must be .int
       if (astruct%inputfile_format/='int  ')then
          call f_err_throw("wrong output format for the coordinate transform", err_name='BIGDFT_RUNTIME_ERROR')
       end if
    else if (direction=='intcar' .or. direction=='carcar') then
       ! output file must be .xyz, .ascii or. .yaml
       if (astruct%inputfile_format/='xyz  ' .and. &
            astruct%inputfile_format/='ascii' .and. &
            astruct%inputfile_format/='yaml ')then
          call f_err_throw("wrong output format for the coordinate transform", err_name='BIGDFT_RUNTIME_ERROR')
       end if
    end if

    na = f_malloc(astruct%nat,id='na')
    nb = f_malloc(astruct%nat,id='nb')
    nc = f_malloc(astruct%nat,id='nc')
    rxyz_int = f_malloc((/ 3, astruct%nat /),id='rxyz_int')

    if (direction=='carint') then
       inquire(file='posinp.fix',exist=file_exists)
       if (file_exists) then
          atoms_ref = f_malloc((/3,astruct%nat/),id='atoms_ref')
          open(unit=123,file='posinp.fix')
          do iat=1,astruct%nat
             read(123,*) atoms_ref(1:3,iat)
          end do
          call get_neighbors(astruct%rxyz, astruct%nat, &
               astruct%ixyz_int(1,:), astruct%ixyz_int(2,:),astruct%ixyz_int(3,:), &
               atoms_ref)
          call f_free(atoms_ref)
       else
          call get_neighbors(astruct%rxyz, astruct%nat, &
               astruct%ixyz_int(1,:), astruct%ixyz_int(2,:),astruct%ixyz_int(3,:))
       end if
       call astruct_dump_to_file(astruct,fileTo(1:irad-1),&
            trim(fcomment) // ' (converted from '// fileFrom //")")

    else if (direction=='intcar' .or. direction=='carcar') then
       call astruct_dump_to_file(astruct,fileTo(1:irad-1),&
            trim(fcomment) // ' (converted from '// fileFrom //")")

    end if

    write(*,*) 'Done.'

    call f_free(na)
    call f_free(nb)
    call f_free(nc)
    call f_free(rxyz_int)
  end subroutine transformCoordinates

  subroutine read_linear_matrix_dense(iunit, ntmb, nat, matrix, rxyz, on_which_atom)
    use module_bigdft_errors
    implicit none

    ! Calling arguments
    integer,intent(in) :: iunit, ntmb, nat
    real(kind=8),dimension(ntmb,ntmb),intent(out) :: matrix
    real(kind=8),dimension(3,nat),intent(out),optional :: rxyz
    integer,dimension(ntmb),intent(out),optional :: on_which_atom

    ! Local variables
    integer :: itmb, jtmb, ii, jj, iat, ntmb_check, nat_check
    logical :: read_rxyz, read_on_which_atom
    real(kind=8),dimension(3) :: dummy
    character(len=128) :: dummy_char

    read_on_which_atom = present(on_which_atom)
    read_rxyz = present(rxyz)

    read(iunit,*) dummy_char, ntmb_check, nat_check
    if (ntmb/=ntmb_check) then
       call f_err_throw('number of basis function specified ('//trim(yaml_toa(ntmb,fmt='(i0)'))//&
            &') does not agree with the number indicated in the file ('//trim(yaml_toa(ntmb_check,fmt='(i0)'))//')',&
            err_name='BIGDFT_RUNTIME_ERROR')
    end if
    if (nat/=nat_check) then
       call f_err_throw('number of atoms specified ('//trim(yaml_toa(nat,fmt='(i0)'))//&
            &') does not agree with the number indicated in the file ('//trim(yaml_toa(nat_check))//')',&
            err_name='BIGDFT_RUNTIME_ERROR')
    end if
    do iat=1,nat
       if (read_rxyz) then
          read(iunit,*) dummy_char, rxyz(1:3,iat)
       else
          read(iunit,*) dummy_char, dummy(1:3)
       end if
    end do

    do itmb=1,ntmb
       do jtmb=1,ntmb
          if(read_on_which_atom .and. jtmb==1) then
             read(iunit,*) ii, jj, matrix(ii,jj), on_which_atom(itmb)
          else
             read(iunit,*) ii, jj, matrix(ii,jj)
          end if
          if (ii/=itmb) call f_err_throw('ii/=itmb',err_name='BIGDFT_RUNTIME_ERROR')
          if (jj/=jtmb) call f_err_throw('jj/=jtmb',err_name='BIGDFT_RUNTIME_ERROR')
       end do
    end do

  end subroutine read_linear_matrix_dense

  subroutine kernelAnalysis(coeff_file, ntmb, norbks, nat, kernel_file)
    use module_precisions
    use module_bigdft_arrays
    use f_utils
    use wrapper_MPI
    use io
    implicit none
    character(len = *), intent(in) :: coeff_file, kernel_file
    integer, intent(in) :: ntmb, norbks, nat

    integer :: iproc, nproc, iunit01, norb_dummy, nspin
    real(gp), dimension(:,:), allocatable :: coeff, denskernel, rxyz
    real(gp), dimension(:), allocatable :: eval
    integer, dimension(:), allocatable :: on_which_atom

    iproc=mpirank()
    nproc=mpisize()
    coeff = f_malloc((/ntmb,norbks/),id='coeff')
    eval = f_malloc(norbks,id='eval')
    denskernel = f_malloc((/ntmb,ntmb/),id='denskernel')
    rxyz = f_malloc((/3,nat/),id='rxyz')
    on_which_atom = f_malloc(ntmb,id='on_which_atom')
    call f_open_file(iunit01, file = coeff_file, binary=.false.)
    ! will not work because if spin==2
    call read_coeff_minbasis(iunit01, .true., iproc, norbks, norb_dummy, ntmb, nspin, coeff, eval, nat, rxyz)
    call f_close(iunit01)
    call f_open_file(iunit01, file = kernel_file, binary=.false.)
    call read_linear_matrix_dense(iunit01, ntmb, nat, denskernel, on_which_atom=on_which_atom)
    call f_close(iunit01)
    call analyze_kernel(ntmb, norbks, nat, coeff, denskernel, rxyz, on_which_atom)
  end subroutine kernelAnalysis

  subroutine extractSubMatrix(matrix_file, ntmb, nat, nsubmatrices)
    use module_precisions
    use module_bigdft_arrays
    use module_bigdft_errors
    use f_utils
    use io
    implicit none
    character(len = *), intent(in) :: matrix_file
    integer, intent(in) :: ntmb, nat, nsubmatrices

    character(len=2) :: num
    integer :: iunit01, isub, iat, itmb, iitmb, jjtmb, jtmb, jat
    real(gp), dimension(:,:), allocatable :: matrix, rxyz
    integer, dimension(:), allocatable :: on_which_atom

    if (mod(ntmb,nsubmatrices)/=0) then
       call f_err_throw('nsubmatrices must be a divisor of the number of basis function',&
            err_name='BIGDFT_RUNETIME_ERROR')
    end if
    matrix = f_malloc((/ntmb,ntmb/),id='matrix')
    on_which_atom = f_malloc(ntmb,id='on_which_atom')
    rxyz = f_malloc((/3,nat/),id='rxyz')
    call f_open_file(iunit01, file=matrix_file, binary=.false.)
    call read_linear_matrix_dense(iunit01, ntmb, nat, matrix, rxyz=rxyz, on_which_atom=on_which_atom)
    call f_close(iunit01)
    do isub=1,nsubmatrices
       write(num,fmt='(i2.2)') isub
       call f_open_file(iunit01, file=matrix_file//'_sub'//num, binary=.false.)
       write(iunit01,'(a,2i10,a)') '#  ',ntmb/nsubmatrices, nat, &
            '    number of basis functions, number of atoms'
       do iat=1,nat
          write(iunit01,'(a,3es24.16)') '#  ',rxyz(1:3,iat)
       end do
       iitmb = 0
       do itmb=isub,ntmb,nsubmatrices
          iitmb = iitmb + 1
          iat = on_which_atom(itmb)
          jjtmb = 0
          do jtmb=isub,ntmb,nsubmatrices
             jjtmb = jjtmb + 1
             jat = on_which_atom(jtmb)
             write(iunit01,'(2(i6,1x),e19.12,2(1x,i6))') iitmb,jjtmb,matrix(itmb,jtmb),iat,jat
          end do
       end do
       call f_close(iunit01)
    end do
  end subroutine extractSubMatrix

  subroutine analyseCoeffs(coeff_file, ntmb, norbks, nat, ncategories)
    use module_precisions
    use module_bigdft_arrays
    use module_bigdft_errors
    use f_utils
    use wrapper_MPI
    use io
    implicit none
    character(len = *), intent(in) :: coeff_file
    integer, intent(in) :: ntmb, norbks, nat, ncategories

    integer :: iproc, nproc, iunit01, iorb, icat, itmb, norb_dummy, nspin
    real(gp) :: tt
    real(gp), dimension(:,:), allocatable :: coeff
    real(gp), dimension(:), allocatable :: coeff_cat, eval

    iproc=mpirank()
    nproc=mpisize()

    coeff = f_malloc((/ntmb,norbks/),id='coeff')
    coeff_cat = f_malloc(ncategories,id='coeff_cat')
    eval = f_malloc(ntmb,id='eval')
    call f_open_file(iunit01, file=coeff_file, binary=.false.)
    ! will not work because if spin==2
    call read_coeff_minbasis(iunit01, .true., iproc, norbks, norb_dummy, ntmb, nspin, coeff, eval, nat)
    call f_close(iunit01)
    do iorb=1,norbks
       do icat=1,ncategories
          tt = 0.d0
          do itmb=icat,ntmb,ncategories
             tt = tt + coeff(itmb,iorb)**2
          end do
          coeff_cat(icat) = tt
       end do
       write(*,'(a,i8,4es12.4)') 'iorb, vals', iorb, coeff_cat(1:ncategories)
    end do
  end subroutine analyseCoeffs

  subroutine peelMatrix(matrix_file, ntmb, nat, cutoff)
    use module_precisions
    use module_bigdft_arrays
    use module_bigdft_errors
    use f_utils
    use io
    use yaml_output
    implicit none
    character(len = *), intent(in) :: matrix_file
    integer, intent(in) :: ntmb, nat
    real(gp), intent(in) :: cutoff

    integer :: iunit01, itmb, iat, jtmb, jat
    integer :: nneighbor_min, nneighbor_max, nneighbor
    real(gp) :: tt
    real(gp), dimension(:,:), allocatable :: matrix, rxyz
    integer, dimension(:), allocatable :: on_which_atom

    matrix = f_malloc((/ntmb,ntmb/),id='matrix')
    on_which_atom = f_malloc(ntmb,id='on_which_atom')
    rxyz = f_malloc((/3,nat/),id='rxyz')
    call f_open_file(iunit01, file=trim(matrix_file), binary=.false.)
    call read_linear_matrix_dense(iunit01, ntmb, nat, matrix, rxyz=rxyz, on_which_atom=on_which_atom)
    call f_close(iunit01)
    nneighbor_min = huge(nneighbor_min)
    nneighbor_max = -huge(nneighbor_max)
    do itmb=1,ntmb
       iat = on_which_atom(itmb)
       nneighbor = 0
       do jtmb=1,ntmb
          jat = on_which_atom(jtmb)
          tt = sqrt((rxyz(1,jat)-rxyz(1,iat))**2 + &
               (rxyz(2,jat)-rxyz(2,iat))**2 + &
               (rxyz(3,jat)-rxyz(3,iat))**2)
          if (tt>cutoff) then
             matrix(jtmb,itmb) = 0.d0
          else
             nneighbor = nneighbor + 1
          end if
       end do
       write(*,*) 'itmb, nneighbor', itmb, nneighbor
       nneighbor_min = min (nneighbor_min,nneighbor)
       nneighbor_max = max (nneighbor_max,nneighbor)
    end do
    call yaml_map('min number of neighbors',nneighbor_min)
    call yaml_map('max number of neighbors',nneighbor_max)
    call f_open_file(iunit01, file=trim(matrix_file)//'_peeled', binary=.false.)
    write(iunit01,'(a,2i10,a)') '#  ',ntmb, nat, &
         '    number of basis functions, number of atoms'
    do iat=1,nat
       write(iunit01,'(a,3es24.16)') '#  ',rxyz(1:3,iat)
    end do
    do itmb=1,ntmb
       iat = on_which_atom(itmb)
       do jtmb=1,ntmb
          jat = on_which_atom(jtmb)
          write(iunit01,'(2(i6,1x),e19.12,2(1x,i6))') itmb,jtmb,matrix(itmb,jtmb),iat,jat
       end do
    end do
    call f_close(iunit01)
  end subroutine peelMatrix

  subroutine multiplyMatrices(amatrix_file, bmatrix_file, ntmb, nat, cmatrix_file)
    use module_precisions
    use module_bigdft_arrays
    use module_bigdft_errors
    use f_utils
    use io
    use yaml_output
    use wrapper_linalg
    implicit none
    character(len = *), intent(in) :: amatrix_file, bmatrix_file, cmatrix_file
    integer, intent(in) :: ntmb, nat

    integer :: iunit01, itmb, iat, jtmb, jat
    real(gp), dimension(:,:), allocatable :: amatrix, bmatrix, cmatrix, rxyz
    integer, dimension(:), allocatable :: on_which_atom

    amatrix = f_malloc((/ntmb,ntmb/),id='amatrix')
    bmatrix = f_malloc((/ntmb,ntmb/),id='bmatrix')
    cmatrix = f_malloc((/ntmb,ntmb/),id='cmatrix')
    on_which_atom = f_malloc(ntmb,id='on_which_atom')
    rxyz = f_malloc((/3,nat/),id='rxyz')
    call f_open_file(iunit01, file=amatrix_file, binary=.false.)
    call read_linear_matrix_dense(iunit01, ntmb, nat, amatrix, rxyz=rxyz, on_which_atom=on_which_atom)
    call f_close(iunit01)
    call f_open_file(iunit01, file=bmatrix_file, binary=.false.)
    call read_linear_matrix_dense(iunit01, ntmb, nat, bmatrix, rxyz=rxyz, on_which_atom=on_which_atom)
    call f_close(iunit01)
    call gemm('n', 'n', ntmb, ntmb, ntmb, 1.d0, amatrix(1,1), ntmb, &
         bmatrix(1,1), ntmb, 0.d0, cmatrix(1,1), ntmb)
    call f_open_file(iunit01, file=cmatrix_file, binary=.false.)
    write(iunit01,'(a,2i10,a)') '#  ',ntmb, nat, &
         '    number of basis functions, number of atoms'
    do iat=1,nat
       write(iunit01,'(a,3es24.16)') '#  ',rxyz(1:3,iat)
    end do
    !cmatrix=0.d0
    do itmb=1,ntmb
       !cmatrix(itmb,itmb)=1.d0
       iat = on_which_atom(itmb)
       do jtmb=1,ntmb
          jat = on_which_atom(jtmb)
          write(iunit01,'(2(i6,1x),e19.12,2(1x,i6))') itmb,jtmb,cmatrix(itmb,jtmb),iat,jat
       end do
    end do
    call f_close(iunit01)
  end subroutine multiplyMatrices

  subroutine matrixPower(inmatrix_file, ntmb, nat, power, outmatrix_file)
    use module_precisions
    use module_bigdft_arrays
    use module_bigdft_errors
    use f_utils
    use io
    use yaml_output
    use wrapper_linalg
    implicit none
    character(len = *), intent(in) :: inmatrix_file, outmatrix_file
    integer, intent(in) :: ntmb, nat
    real(gp), intent(in) :: power

    integer :: iunit01, itmb, iat, jtmb, jat, lwork, info
    real(gp), dimension(:,:), allocatable :: amatrix, bmatrix, rxyz, tempArr
    integer, dimension(:), allocatable :: on_which_atom
    real(gp), dimension(:), allocatable :: eval, work
    
    amatrix = f_malloc((/ntmb,ntmb/),id='amatrix')
    bmatrix = f_malloc((/ntmb,ntmb/),id='bmatrix')
    on_which_atom = f_malloc(ntmb,id='on_which_atom')
    rxyz = f_malloc((/3,nat/),id='rxyz')
    eval = f_malloc(ntmb,id='eval')
    call f_open_file(iunit01, file=inmatrix_file, binary=.false.)
    call read_linear_matrix_dense(iunit01, ntmb, nat, amatrix, rxyz=rxyz, on_which_atom=on_which_atom)
    call f_close(iunit01)

    lwork = 10*ntmb
    work = f_malloc(lwork,id='work')
    tempArr=f_malloc((/ntmb,ntmb/), id='tempArr')
    call dsyev('v', 'l', ntmb, amatrix(1,1), ntmb, eval, work, lwork, info)
    do itmb=1,ntmb
       do jtmb=1,ntmb
          tempArr(jtmb,itmb)=amatrix(jtmb,itmb)*eval(itmb)**power
       end do
    end do
    call gemm('n', 't', ntmb, ntmb, ntmb, 1.d0, amatrix(1,1), &
         ntmb, tempArr(1,1), ntmb, 0.d0, bmatrix(1,1), ntmb)
    call f_free(tempArr)
    call f_open_file(iunit01, file=outmatrix_file, binary=.false.)
    write(iunit01,'(a,2i10,a)') '#  ',ntmb, nat, &
         '    number of basis functions, number of atoms'
    do iat=1,nat
       write(iunit01,'(a,3es24.16)') '#  ',rxyz(1:3,iat)
    end do
    !cmatrix=0.d0
    do itmb=1,ntmb
       !cmatrix(itmb,itmb)=1.d0
       iat = on_which_atom(itmb)
       do jtmb=1,ntmb
          jat = on_which_atom(jtmb)
          write(iunit01,'(2(i6,1x),e19.12,2(1x,i6))') itmb,jtmb,bmatrix(itmb,jtmb),iat,jat
       end do
    end do
    call f_close(iunit01)
  end subroutine matrixPower

  subroutine suggestCutoff(posinp_file)
    use module_precisions
    use module_bigdft_arrays
    use module_atoms
    implicit none
    character(len = *), intent(in) :: posinp_file

    integer :: iat, iiat, iitype, itype, jat, jjat, jtype
    type(atomic_structure) :: astruct
    character(len=1024) :: fcomment
    real(gp) :: energy, d
    real(gp), dimension(:), allocatable :: rcov, d2min_list, dtype
    real(gp), dimension(:,:), pointer :: fxyz
    real(gp), dimension(:,:), allocatable :: d1min_list
    integer, dimension(:,:), allocatable :: imin_list

    call set_astruct_from_file(posinp_file,0,astruct,fcomment,energy,fxyz)
    rcov = f_malloc(max(5, astruct%ntypes),id='rcov')
    rcov(1) = 3.30d0
    rcov(2) = 2.50d0
    rcov(3) = 1.45d0
    rcov(4) = 1.42d0
    rcov(5) = 0.75d0
    !rcov(1) = 1.45d0

    d1min_list = f_malloc((/2,astruct%nat/),id='d2min_list')
    d2min_list = f_malloc(astruct%nat,id='d1min_list')
    imin_list = f_malloc((/2,astruct%nat/),id='imin_list')
    dtype = f_malloc(astruct%ntypes,id='dtype')

    d1min_list = huge(d1min_list)
    imin_list = 0
    do iat=1,astruct%nat
       itype = astruct%iatype(iat)
       do jat=1,astruct%nat
          jtype = astruct%iatype(jat)
          if (jat/=iat) then
             !if (rcov(jtype)<=rcov(itype)) then
             d = (astruct%rxyz(1,jat)-astruct%rxyz(1,iat))**2 + &
                  (astruct%rxyz(2,jat)-astruct%rxyz(2,iat))**2 + &
                  (astruct%rxyz(3,jat)-astruct%rxyz(3,iat))**2
             d = sqrt(d)
             !else
             !    d = 3.d0*rcov(itype)
             !end if
             if (d<d1min_list(1,iat)) then
                d1min_list(2,iat) = d1min_list(1,iat)
                d1min_list(1,iat) = d
                imin_list(2,iat) = imin_list(1,iat)
                imin_list(1,iat) = jat
             else if (d<d1min_list(2,iat)) then
                d1min_list(2,iat) = d
                imin_list(2,iat) = jat
             end if
          end if
       end do
    end do

    d2min_list = huge(d2min_list)
    do iat=1,astruct%nat
       iiat = imin_list(1,iat)
       itype = astruct%iatype(iat)
       iitype = astruct%iatype(iiat)
       write(*,'(a,i5,2es12.4)') 'iat, d1min_list(1:2,iat)', iat, d1min_list(1:2,iat)
       do jat=1,2
          jjat = imin_list(jat,iiat)
          jtype = astruct%iatype(jjat)
          if (jjat/=iat) then
             !if (rcov(jtype)<=rcov(itype)) then
             if (rcov(iitype)<=rcov(itype)) then
                d = (astruct%rxyz(1,iat)-astruct%rxyz(1,jjat))**2 + &
                     (astruct%rxyz(2,iat)-astruct%rxyz(2,jjat))**2 + &
                     (astruct%rxyz(3,iat)-astruct%rxyz(3,jjat))**2
                d = sqrt(d)
             else
                d = 3.d0*rcov(itype)
             end if
             write(*,'(a,5i8,es12.4)') 'itype, iat, iiat, jat, jjat, d', itype, iat, iiat, jat, jjat, d
             d2min_list(iat) = d
          end if
       end do
    end do

    dtype = -huge(dtype)
    do iat=1,astruct%nat
       itype = astruct%iatype(iat)
       d = d2min_list(iat)
       dtype(itype) = max(d,dtype(itype))
    end do

    do itype=1,astruct%ntypes
       write(*,'(a,i7,a,f7.2,es12.4)') 'itype, name, rcov dtype(itype)', &
            itype, astruct%atomnames(itype), rcov(itype), dtype(itype)
    end do
  end subroutine suggestCutoff

  subroutine minimiseBox(radical)
    use module_precisions
    use dictionaries
    use bigdft_run
    use at_domain
    use module_atoms
    implicit none
    character(len = *), intent(in) :: radical

    type(dictionary), pointer :: run
    type(run_objects) :: runObj
    character(len=40) :: comment
    integer :: ierror

    nullify(run)
    call bigdft_set_run_properties(run, run_id = radical, &
         run_from_files = .true., log_to_disk = .false., minimal_file=radical)

    call run_objects_init(runObj, run)
    call dict_free(run)

    if (domain_geocode(runObj%atoms%astruct%dom) =='F') then
       call optimise_volume(runObj%atoms,&
            & runObj%inputs%crmult,runObj%inputs%frmult,&
            & runObj%inputs%hx,runObj%inputs%hy,runObj%inputs%hz,&
            & runObj%atoms%astruct%rxyz)
    else
       call shift_periodic_directions(runObj%atoms,runObj%atoms%astruct%rxyz,runObj%atoms%radii_cf)
    end if
    write(*,'(1x,a)')'Writing optimised positions in file posopt.[xyz,ascii]...'
    write(comment,'(a)')'POSITIONS IN OPTIMIZED CELL '

    call astruct_dump_to_file(runObj%atoms%astruct,'posopt',trim(comment))

    !remove the directory which has been created if it is possible
    call deldir(runObj%inputs%dir_output,len(trim(runObj%inputs%dir_output)),ierror)

    call free_run_objects(runObj)
  end subroutine minimiseBox

  !> Rotate the molecule via an orthogonal matrix in order to minimise the
  !! volume of the cubic cell
  subroutine optimise_volume(atoms,crmult,frmult,hx,hy,hz,rxyz)
    use module_precisions
    use module_types
    use module_bigdft_arrays
    use locregs
    use at_domain, only: domain_volume
    implicit none
    type(atoms_data), intent(inout) :: atoms
    real(gp), intent(in) :: crmult,frmult
    real(gp), intent(inout) :: hx,hy,hz
    real(gp), dimension(3,atoms%astruct%nat), intent(inout) :: rxyz
    !local variables
    character(len=*), parameter :: subname='optimise_volume'
    integer :: iat,it,i
    real(gp) :: x,y,z,vol,tx,ty,tz,tvol,s,diag,dmax
    type(locreg_descriptors) :: Glr
    real(gp), dimension(3,3) :: urot
    real(gp), dimension(:,:), allocatable :: txyz

    txyz = f_malloc((/ 3, atoms%astruct%nat /),id='txyz')
    call system_size(atoms,rxyz,crmult,frmult,hx,hy,hz,.false.,Glr)
    !call volume(nat,rxyz,vol)
    !vol=atoms%astruct%cell_dim(1)*atoms%astruct%cell_dim(2)*atoms%astruct%cell_dim(3)
    vol=domain_volume(atoms%astruct%cell_dim,atoms%astruct%dom)
    write(*,'(1x,a,1pe16.8)')'Initial volume (Bohr^3)',vol

    it=0
    diag=1.d-2 ! initial small diagonal element allows for search over all angles
    loop_rotations: do  ! loop over all trial rotations
       diag=diag*1.0001_gp ! increase diag to search over smaller angles
       it=it+1
       if (diag > 100._gp) exit loop_rotations ! smaller angle rotations do not make sense

       ! create a random orthogonal (rotation) matrix
       call random_number(urot)
       urot(:,:)=urot(:,:)-.5_gp
       do i=1,3
          urot(i,i)=urot(i,i)+diag
       enddo

       s=urot(1,1)**2+urot(2,1)**2+urot(3,1)**2
       s=1._gp/sqrt(s)
       urot(:,1)=s*urot(:,1)

       s=urot(1,1)*urot(1,2)+urot(2,1)*urot(2,2)+urot(3,1)*urot(3,2)
       urot(:,2)=urot(:,2)-s*urot(:,1)
       s=urot(1,2)**2+urot(2,2)**2+urot(3,2)**2
       s=1._gp/sqrt(s)
       urot(:,2)=s*urot(:,2)

       s=urot(1,1)*urot(1,3)+urot(2,1)*urot(2,3)+urot(3,1)*urot(3,3)
       urot(:,3)=urot(:,3)-s*urot(:,1)
       s=urot(1,2)*urot(1,3)+urot(2,2)*urot(2,3)+urot(3,2)*urot(3,3)
       urot(:,3)=urot(:,3)-s*urot(:,2)
       s=urot(1,3)**2+urot(2,3)**2+urot(3,3)**2
       s=1._gp/sqrt(s)
       urot(:,3)=s*urot(:,3)

       ! eliminate reflections
       if (urot(1,1) <= 0._gp) urot(:,1)=-urot(:,1)
       if (urot(2,2) <= 0._gp) urot(:,2)=-urot(:,2)
       if (urot(3,3) <= 0._gp) urot(:,3)=-urot(:,3)

       ! apply the rotation to all atomic positions!
       do iat=1,atoms%astruct%nat
          x=rxyz(1,iat)
          y=rxyz(2,iat)
          z=rxyz(3,iat)

          txyz(:,iat)=x*urot(:,1)+y*urot(:,2)+z*urot(:,3)
       enddo

       call system_size(atoms,txyz,crmult,frmult,hx,hy,hz,.false.,Glr)
       !tvol=atoms%astruct%cell_dim(1)*atoms%astruct%cell_dim(2)*atoms%astruct%cell_dim(3)
       tvol=domain_volume(atoms%astruct%cell_dim,atoms%astruct%dom)
       !call volume(nat,txyz,tvol)
       if (tvol < vol) then
          write(*,'(1x,a,1pe16.8,1x,i0,1x,f15.5)')'Found new best volume: ',tvol,it,diag
          rxyz(:,:)=txyz(:,:)
          vol=tvol
          dmax=max(atoms%astruct%cell_dim(1),atoms%astruct%cell_dim(2),atoms%astruct%cell_dim(3))
          ! if box longest along x switch x and z
          if (atoms%astruct%cell_dim(1) == dmax)  then
             do  iat=1,atoms%astruct%nat
                tx=rxyz(1,iat)
                tz=rxyz(3,iat)

                rxyz(1,iat)=tz
                rxyz(3,iat)=tx
             enddo
             ! if box longest along y switch y and z
          else if (atoms%astruct%cell_dim(2) == dmax .and. atoms%astruct%cell_dim(1) /= dmax)  then
             do  iat=1,atoms%astruct%nat
                ty=rxyz(2,iat)
                tz=rxyz(3,iat)

                rxyz(2,iat)=tz
                rxyz(3,iat)=ty
             enddo
          endif
       endif
    end do loop_rotations

    call f_free(txyz)

  END SUBROUTINE optimise_volume


  !> Add a shift in the periodic directions such that the system
  !! uses as less as possible the modulo operation
  subroutine shift_periodic_directions(at,rxyz,radii_cf)
    use module_precisions
    use module_types
    use at_domain, only: domain_geocode
    use module_bigdft_arrays
    implicit none
    type(atoms_data), intent(inout) :: at
    real(gp), dimension(at%astruct%ntypes,3), intent(in) :: radii_cf
    real(gp), dimension(3,at%astruct%nat), intent(inout) :: rxyz
    !local variables
    character(len=*), parameter :: subname='shift_periodic_directions'
    integer :: iat,i,ityp
    real(gp) :: vol,tvol,maxsh,shiftx,shifty,shiftz
    real(gp), dimension(:,:), allocatable :: txyz

    !calculate maximum shift between these values
    !this is taken as five times the coarse radius around atoms
    maxsh=0.0_gp
    do ityp=1,at%astruct%ntypes
       maxsh=max(maxsh,5_gp*radii_cf(ityp,1))
    end do

    txyz = f_malloc((/ 3, at%astruct%nat /),id='txyz')

    call calc_vol(domain_geocode(at%astruct%dom),at%astruct%nat,rxyz,vol)

    if (domain_geocode(at%astruct%dom) /= 'F') then
       loop_shiftx: do i=1,5000 ! loop over all trial rotations
          ! create a random orthogonal (rotation) matrix
          call random_number(shiftx)

          !apply the shift to all atomic positions taking into account the modulo operation
          do iat=1,at%astruct%nat
             txyz(1,iat)=modulo(rxyz(1,iat)+shiftx*maxsh,at%astruct%cell_dim(1))
             txyz(2,iat)=rxyz(2,iat)
             txyz(3,iat)=rxyz(3,iat)
          end do

          call calc_vol(domain_geocode(at%astruct%dom),at%astruct%nat,txyz,tvol)
          !print *,'vol',tvol

          if (tvol < vol) then
             write(*,'(1x,a,1pe16.8,1x,i0,1x,f15.5)')'Found new best volume: ',tvol
             rxyz(:,:)=txyz(:,:)
             vol=tvol
          endif
       end do loop_shiftx
    end if

    if (domain_geocode(at%astruct%dom) == 'P') then
       loop_shifty: do i=1,5000 ! loop over all trial rotations
          ! create a random orthogonal (rotation) matrix
          call random_number(shifty)

          !apply the shift to all atomic positions taking into account the modulo operation
          do iat=1,at%astruct%nat
             txyz(1,iat)=rxyz(1,iat)
             txyz(2,iat)=modulo(rxyz(2,iat)+shifty*maxsh,at%astruct%cell_dim(2))
             txyz(3,iat)=rxyz(3,iat)
          end do

          call calc_vol(domain_geocode(at%astruct%dom),at%astruct%nat,txyz,tvol)

          if (tvol < vol) then
             write(*,'(1x,a,1pe16.8,1x,i0,1x,f15.5)')'Found new best volume: ',tvol
             rxyz(:,:)=txyz(:,:)
             vol=tvol
          endif
       end do loop_shifty
    end if

    if (domain_geocode(at%astruct%dom) /= 'F') then
       loop_shiftz: do i=1,5000 ! loop over all trial rotations
          ! create a random orthogonal (rotation) matrix
          call random_number(shiftz)

          !apply the shift to all atomic positions taking into account the modulo operation
          do iat=1,at%astruct%nat
             txyz(1,iat)=rxyz(1,iat)
             txyz(2,iat)=rxyz(2,iat)
             txyz(3,iat)=modulo(rxyz(3,iat)+shiftz*maxsh,at%astruct%cell_dim(3))
          end do

          call calc_vol(domain_geocode(at%astruct%dom),at%astruct%nat,txyz,tvol)

          if (tvol < vol) then
             write(*,'(1x,a,1pe16.8,1x,i0,1x,f15.5)')'Found new best volume: ',tvol
             rxyz(:,:)=txyz(:,:)
             vol=tvol
          endif
       end do loop_shiftz
    end if

    call f_free(txyz)

  END SUBROUTINE shift_periodic_directions
  
  !> Calculate the extremes of the boxes taking into account the spheres around the atoms
  subroutine calc_vol(geocode,nat,rxyz,vol)
    use module_precisions
    implicit none
    character(len=1), intent(in) :: geocode !< @copydoc poisson_solver::doc::geocode
    integer, intent(in) :: nat
    real(gp), dimension(3,nat), intent(in) :: rxyz
    real(gp), intent(out) :: vol
    !local variables
    integer :: iat
    real(gp) :: cxmin,cxmax,cymin,cymax,czmin,czmax

    cxmax=-1.e10_gp
    cxmin=1.e10_gp

    cymax=-1.e10_gp
    cymin=1.e10_gp

    czmax=-1.e10_gp
    czmin=1.e10_gp

    do iat=1,nat
       cxmax=max(cxmax,rxyz(1,iat))
       cxmin=min(cxmin,rxyz(1,iat))

       cymax=max(cymax,rxyz(2,iat))
       cymin=min(cymin,rxyz(2,iat))

       czmax=max(czmax,rxyz(3,iat))
       czmin=min(czmin,rxyz(3,iat))
    enddo
    !print *,cxmax,cxmin,cymax,cymin,czmax,czmin
    !now calculate the volume for the periodic part
    if (geocode == 'P') then
       vol=(cxmax-cxmin)*(cymax-cymin)*(czmax-czmin)
    else if (geocode == 'S') then
       vol=(cxmax-cxmin)*(czmax-czmin)
    else if (geocode == 'W') then
       vol=(czmax-czmin)
    end if

  END SUBROUTINE calc_vol

  subroutine exportWavefunction(radical, filename_wfn, iband, ikpt, ispin, ispinor)
    use module_precisions
    use dictionaries
    use bigdft_run
    use module_bigdft_arrays
    use locregs
    use module_interfaces
    use io
    implicit none
    character(len = *), intent(in) :: radical, filename_wfn
    integer, intent(in), optional :: iband, ikpt, ispin, ispinor

    type(dictionary), pointer :: run
    type(run_objects) :: runObj
    type(locreg_descriptors) :: llr
    real(wp), dimension(:), pointer :: psi
    real(gp), dimension(:,:), pointer :: rxyz
    type(io_descriptor) :: descr

    nullify(run)
    call bigdft_set_run_properties(run, run_id = radical, &
         run_from_files = .true., log_to_disk = .false.)

    call run_objects_init(runObj, run)
    call dict_free(run)

    descr = io_read_description(filename_wfn, runObj%atoms%astruct%nat)
    psi => io_descr_phi(descr, llr, ikpt, iband, ispin, ispinor)
    rxyz => io_descr_rxyz(descr)
    call io_deallocate_descriptor(descr)

    call ensure_locreg_bounds(llr)
    call plot_wf(.false., filename_wfn, 1, runObj%atoms, 1.0_wp, llr, &
         & llr%mesh_coarse%hgrids, rxyz, psi)
    call f_free_ptr(rxyz)

    call f_free_ptr(psi)
    call deallocate_locreg_descriptors(llr)
    call free_run_objects(runObj)
  end subroutine exportWavefunction

  subroutine exportProjector(radical, filename_proj)
    use module_precisions
    use dictionaries
    use bigdft_run
    use module_bigdft_arrays
    use at_domain
    use module_atoms
    use module_types
    use module_fragments
    use public_enums
    use f_enums
    use module_interfaces
    use locregs
    use yaml_output
    use communications_base, only: deallocate_comms
    use psp_projectors_base, only: free_DFT_PSP_projectors
    use io
    use orbitalbasis
    implicit none
    character(len = *), intent(in) :: radical, filename_proj

    type(dictionary), pointer :: run
    type(run_objects) :: runObj
    integer :: input_wf_format, iat, icplx, ikpt, iproj, i
    type(system_fragment), dimension(:), pointer :: ref_frags
    type(DFT_PSP_projectors) :: nlpsp
    type(f_enumerator) :: inputpsi
    character(len = 256) :: fileout
    type(orbital_basis) :: ob
    type(io_descriptor) :: descr
    real(wp), dimension(:), pointer :: phi
    real(gp), dimension(:,:), pointer :: rxyz
    type(locreg_descriptors) :: plr

    nullify(run)
    call bigdft_set_run_properties(run, run_id = radical, &
         run_from_files = .true., log_to_disk = .false., minimal_file=radical)

    call run_objects_init(runObj, run)
    call dict_free(run)

    inputpsi = runObj%inputs%inputPsiId
    call system_initialization(0, 1, .true.,inputpsi, input_wf_format, .true., &
         & runObj%inputs, runObj%atoms, runObj%atoms%astruct%rxyz, runObj%rst%GPU%OCLconv, &
         & runObj%rst%KSwfn%orbs, runObj%rst%tmb%npsidim_orbs, runObj%rst%tmb%npsidim_comp, &
         & runObj%rst%tmb%orbs, runObj%rst%KSwfn%Lzd, runObj%rst%tmb%Lzd, nlpsp, runObj%rst%KSwfn%comms, &
         & ref_frags, output_grid = .false.)
    call free_DFT_PSP_projectors(nlpsp)

    call orbital_basis_associate(ob,orbs=runObj%rst%KSwfn%orbs,&
         & Lzd=runObj%rst%KSwfn%Lzd,id='memguess')
    call createProjectorsArrays(runObj%rst%KSwfn%Lzd%Glr, &
         & runObj%atoms%astruct%rxyz,runObj%atoms,ob%orbs, &
         & runObj%inputs%frmult,runObj%inputs%frmult, &
         & runObj%inputs%projection,.false.,nlpsp,.true.)
    nlpsp%on_the_fly = .true.
    call orbital_basis_release(ob)

    descr = io_read_description(filename_proj, runObj%atoms%astruct%nat)
    icplx = 1
    i = index(filename_proj, "-", back = .true.)+1
    if (i > 1 .and. filename_proj(i:i) == "R") icplx = 1
    if (i > 1 .and. filename_proj(i:i) == "I") icplx = 2
    phi => io_descr_phi(descr, 1, plr, icplx)
    rxyz => io_descr_rxyz(descr)
    call plot_wf(.false.,trim(fileout),1,runObj%atoms,1.0_wp,plr, &
         & plr%mesh_coarse%hgrids,rxyz,phi)
    call f_free_ptr(rxyz)
    call f_free_ptr(phi)
    call deallocate_locreg_descriptors(plr)
    call io_deallocate_descriptor(descr)

    ! De-allocations
    call deallocate_Lzd_except_Glr(runObj%rst%KSwfn%Lzd)
    call deallocate_comms(runObj%rst%KSwfn%comms)
    call deallocate_orbs(runObj%rst%KSwfn%orbs)

    call free_run_objects(runObj)
  end subroutine exportProjector

  subroutine atomicWavefunctions(radical, ng)
    use module_precisions
    use dictionaries
    use bigdft_run
    use module_bigdft_arrays
    use at_domain
    use module_atoms
    use module_types
    use module_fragments
    use public_enums
    use f_enums
    use module_interfaces
    use locregs
    use yaml_output
    use communications_base, only: deallocate_comms
    use psp_projectors_base, only: free_DFT_PSP_projectors
    use io
    use gaussians
    implicit none
    character(len = *), intent(in) :: radical
    integer, intent(in) :: ng

    type(dictionary), pointer :: run
    type(run_objects) :: runObj
    integer :: input_wf_format
    type(system_fragment), dimension(:), pointer :: ref_frags
    type(DFT_PSP_projectors) :: nlpsp
    type(f_enumerator) :: inputpsi
    type(gaussian_basis) :: G !basis for davidson IG
    real(wp), dimension(:), allocatable :: rhoexpo
    real(wp), dimension(:,:,:,:), pointer :: rhocoeff
    real(gp), dimension(:), pointer :: gbd_occ

    nullify(run)
    call bigdft_set_run_properties(run, run_id = radical, &
         run_from_files = .true., log_to_disk = .false., minimal_file=radical)

    call run_objects_init(runObj, run)
    call dict_free(run)

    inputpsi = runObj%inputs%inputPsiId
    call system_initialization(0, 1, .true.,inputpsi, input_wf_format, .true., &
         & runObj%inputs, runObj%atoms, runObj%atoms%astruct%rxyz, runObj%rst%GPU%OCLconv, &
         & runObj%rst%KSwfn%orbs, runObj%rst%tmb%npsidim_orbs, runObj%rst%tmb%npsidim_comp, &
         & runObj%rst%tmb%orbs, runObj%rst%KSwfn%Lzd, runObj%rst%tmb%Lzd, nlpsp, runObj%rst%KSwfn%comms, &
         & ref_frags, output_grid = .false.)
    call free_DFT_PSP_projectors(nlpsp)

    !here the treatment of the AE Core charge density
    !number of gaussians defined in the input of memguess
    !ng=31
    !plot the wavefunctions for the pseudo atom
    nullify(G%rxyz)
    call gaussian_pswf_basis(ng,.false.,0,runObj%inputs%nspin,runObj%atoms,runObj%atoms%astruct%rxyz,G,gbd_occ)
    !for the moment multiply the number of coefficients for each channel
    rhocoeff = f_malloc_ptr((/ (ng*(ng+1))/2, 4, 1, 1 /),id='rhocoeff')
    rhoexpo = f_malloc((ng*(ng+1))/2,id='rhoexpo')

    call plot_gatom_basis('gatom',1,ng,G,gbd_occ,rhocoeff,rhoexpo)

    if (associated(gbd_occ)) then
       call f_free_ptr(gbd_occ)
       nullify(gbd_occ)
    end if
    !deallocate the gaussian basis descriptors
    call deallocate_gwf(G)

!!$  !plot the wavefunctions for the AE atom
!!$  !not possible, the code should recognize the AE eleconf
!!$  call to_zero(35,runObj%atoms%psppar(0,0,runObj%atoms%astruct%iatype(1)))
!!$  runObj%atoms%psppar(0,0,runObj%atoms%astruct%iatype(1))=0.01_gp
!!$  nullify(G%rxyz)
!!$  call gaussian_pswf_basis(ng,.false.,0,runObj%inputs%nspin,atoms,runObj%atoms%astruct%rxyz,G,gbd_occ)
!!$  !for the moment multiply the number of coefficients for each channel
!!$  allocate(rhocoeff((ng*(ng+1))/2,4+ndebug),stat=i_stat)
!!$  call memocc(i_stat,rhocoeff,'rhocoeff',subname)
!!$  allocate(rhoexpo((ng*(ng+1))/2+ndebug),stat=i_stat)
!!$  call memocc(i_stat,rhoexpo,'rhoexpo',subname)
!!$
!!$  call plot_gatom_basis('all-elec',1,ng,G,gbd_occ,rhocoeff,rhoexpo)
!!$
!!$  if (associated(gbd_occ)) then
!!$     i_all=-product(shape(gbd_occ))*kind(gbd_occ)
!!$     deallocate(gbd_occ,stat=i_stat)
!!$     call memocc(i_stat,i_all,'gbd_occ',subname)
!!$     nullify(gbd_occ)
!!$  end if
!!$  !deallocate the gaussian basis descriptors
!!$  call deallocate_gwf(G)

    call f_free(rhoexpo)
    call f_free_ptr(rhocoeff)
    ! De-allocations
    call deallocate_Lzd_except_Glr(runObj%rst%KSwfn%Lzd)
    call deallocate_comms(runObj%rst%KSwfn%comms)
    call deallocate_orbs(runObj%rst%KSwfn%orbs)

    call free_run_objects(runObj)
  end subroutine atomicWavefunctions

!!$  subroutine write_gaussian_peaks(norb,coeff,energy,sigma,npts,filename)
!!$
!!$    use module_precisions
!!$    use module_bigdft_output
!!$
!!$    implicit none
!!$    !In/out variables
!!$    integer, intent(in) :: norb, npts
!!$    real(gp), dimension(norb), intent(in) :: coeff
!!$    real(gp), dimension(norb), intent(in) :: energy
!!$    real(wp), intent(in) :: sigma
!!$    character(len=*), intent(in) :: filename
!!$
!!$    !local variables
!!$    integer :: ih, iunit, ie
!!$    real(wp) :: e_grid,emin, emax, he, towrite
!!$
!!$
!!$    !Define the grid of energy
!!$    !e_grid = f_malloc(npts,id='e_grid') !grid of energy
!!$    emin=minval(energy,norb)-2*sigma
!!$    emax=maxval(energy,norb)+2*sigma
!!$    he=(emax-emin)/(npts-1) !energy gridspace
!!$
!!$    !open output file
!!$    iunit=99
!!$    call f_open_file(iunit, file=filename, binary=.false.)
!!$    !loop over the grid points
!!$    do ih=1, npts
!!$       !e_grid(ih) = emin + (ih-1)*he
!!$       e_grid = emin + (ih-1)*he
!!$       towrite=0.0d0
!!$       !loop over the energies: add the gaussian contribution of each energy at the current grid-point e_grid.
!!$       do ie=1, norb
!!$          towrite = towrite + coeff(ie)*exp( - (e_grid-energy(ie))**2 / (2*sigma**2) )
!!$       end do
!!$       !write the computed value at the ih-th grid point
!!$       !      write(iunit,'(2(E15.8E2,1x))') e_grid, towrite
!!$       write(iunit,'(a)') ' '//trim(yaml_toa(e_grid,fmt='(e15.8e2)'))//trim(yaml_toa(towrite,fmt='(e15.8e2)'))
!!$
!!$    end do
!!$    !close output file
!!$    call f_close(iunit)
!!$  end subroutine write_gaussian_peaks

  subroutine testGPU(radical, nproc, ntimes, norbgpu)
    use module_precisions
    use dictionaries
    use bigdft_run
    use module_bigdft_arrays
    use at_domain
    use module_atoms
    use module_types
    use module_fragments
    use public_enums
    use f_enums
    use module_interfaces
    use locregs
    use yaml_output
    use communications_base, only: deallocate_comms
    use psp_projectors_base, only: free_DFT_PSP_projectors
    implicit none
    character(len = *), intent(in) :: radical
    integer, intent(in) :: nproc, ntimes, norbgpu

    type(dictionary), pointer :: run
    type(run_objects) :: runObj
    integer :: input_wf_format, iorb, norb, norbu, norbd, nspin, nspinor
    type(system_fragment), dimension(:), pointer :: ref_frags
    type(DFT_PSP_projectors) :: nlpsp
    type(f_enumerator) :: inputpsi
    type(orbitals_data) :: orbstst

    nullify(run)
    call bigdft_set_run_properties(run, run_id = radical, &
         run_from_files = .true., log_to_disk = .false., minimal_file=radical)

    call run_objects_init(runObj, run)
    call dict_free(run)

    inputpsi = runObj%inputs%inputPsiId
    call system_initialization(0, nproc, .true.,inputpsi, input_wf_format, .true., &
         & runObj%inputs, runObj%atoms, runObj%atoms%astruct%rxyz, runObj%rst%GPU%OCLconv, &
         & runObj%rst%KSwfn%orbs, runObj%rst%tmb%npsidim_orbs, runObj%rst%tmb%npsidim_comp, &
         & runObj%rst%tmb%orbs, runObj%rst%KSwfn%Lzd, runObj%rst%tmb%Lzd, nlpsp, runObj%rst%KSwfn%comms, &
         & ref_frags, output_grid = .false.)
    call free_DFT_PSP_projectors(nlpsp)

    !test the hamiltonian in CPU or GPU
    !create the orbitals data structure for one orbital
    !test orbitals
    nspin=1
    if (norbgpu == 0) then
       norb=runObj%rst%KSwfn%orbs%norb
    else
       norb=norbgpu
    end if
    norbu=norb
    norbd=0
    nspinor=1

    call orbitals_descriptors(0,nproc,norb,norbu,norbd,runObj%inputs%nspin,nspinor, &
         runObj%inputs%gen_nkpt,runObj%inputs%gen_kpt,runObj%inputs%gen_wkpt,orbstst,LINEAR_PARTITION_NONE)
    orbstst%eval = f_malloc_ptr(orbstst%norbp,id='orbstst%eval')
    do iorb=1,orbstst%norbp
       orbstst%eval(iorb)=-0.5_gp
    end do

    do iorb=1,orbstst%norb
       orbstst%occup(iorb)=1.0_gp
       orbstst%spinsgn(iorb)=1.0_gp
    end do

    call check_linear_and_create_Lzd(0,1,runObj%inputs%linear,runObj%rst%KSwfn%Lzd,&
         & runObj%atoms,orbstst,runObj%inputs%nspin,runObj%atoms%astruct%rxyz)

    !for the given processor (this is only the cubic strategy)
    orbstst%npsidim_orbs=array_dim(runObj%rst%KSwfn%Lzd%Glr)*orbstst%norbp*orbstst%nspinor
    orbstst%npsidim_comp=1


    call compare_cpu_gpu_hamiltonian(0,1,runObj%inputs%matacc,runObj%atoms,&
         orbstst,nspin,runObj%inputs%ncong,runObj%inputs%ixc,&
         runObj%rst%KSwfn%Lzd,runObj%rst%KSwfn%Lzd%glr%mesh_coarse%hgrids(1),&
         runObj%rst%KSwfn%Lzd%glr%mesh_coarse%hgrids(2),&
         runObj%rst%KSwfn%Lzd%glr%mesh_coarse%hgrids(3),runObj%atoms%astruct%rxyz,ntimes)

    call deallocate_orbs(orbstst)


    call f_free_ptr(orbstst%eval)

    ! De-allocations
    call deallocate_Lzd_except_Glr(runObj%rst%KSwfn%Lzd)
    call deallocate_comms(runObj%rst%KSwfn%comms)
    call deallocate_orbs(runObj%rst%KSwfn%orbs)

    call free_run_objects(runObj)
  end subroutine testGPU

  subroutine compare_cpu_gpu_hamiltonian(iproc,nproc,matacc,at,orbs,&
       nspin,ixc,ncong,Lzd,hx,hy,hz,rxyz,ntimes)
    use module_precisions
    use module_types
    use module_bigdft_arrays
    use module_bigdft_config
    use module_interfaces, only: gaussian_pswf_basis
    use Poisson_Solver, except_dp => dp, except_gp => gp
    use gaussians, only: gaussian_basis, deallocate_gwf
    use module_xc
    use module_input_keys
    use module_dpbox
    use locregs
    use locreg_operations, only: confpot_data
    use at_domain, only: domain_geocode
    use wrapper_linalg
    use module_bigdft_mpi
    implicit none
    integer, intent(in) :: iproc,nproc,nspin,ncong,ixc,ntimes
    real(gp), intent(in) :: hx,hy,hz
    type(material_acceleration), intent(in) :: matacc
    type(atoms_data), intent(in) :: at
    type(orbitals_data), intent(inout) :: orbs
    type(local_zone_descriptors), intent(inout) :: Lzd
    real(gp), dimension(3,at%astruct%nat), intent(in) :: rxyz
    !local variables
    character(len=*), parameter :: subname='compare_cpu_gpu_hamiltonian'
    logical :: rsflag
    integer :: icoeff,i1,i2,i3,ispin,j
    integer :: iorb,jproc,nrhotot,nspinn,nvctrp
    integer(kind=8) :: itsc0,itsc1
    real(kind=4) :: tt
    real(gp) :: ttd,x,y,z,r2,arg,sigma2,ekin_sum,epot_sum,ekinGPU,epotGPU,gnrm,gnrm_zero,gnrmGPU
    real(gp) :: Rden,Rham,Rgemm,Rsyrk,Rprec,eSIC_DC
    real(kind=8) :: CPUtime,GPUtime
    type(gaussian_basis) :: G
    type(GPU_pointers) :: GPU
    type(xc_info) :: xc
    real(wp), dimension(:,:,:,:), allocatable :: pot,rho
    real(wp), dimension(:), pointer:: pottmp
    real(wp), dimension(:,:), allocatable :: gaucoeffs,psi,hpsi
    real(wp), dimension(:,:,:), allocatable :: overlap
    real(wp), dimension(:), pointer :: gbd_occ
    type(coulomb_operator) :: fake_pkernelSIC
    type(confpot_data), dimension(orbs%norbp) :: confdatarr
    type(denspot_distribution) :: dpbox

    call default_confinement_data(confdatarr,orbs%norbp)

    !nullify pkernelSIC pointer
    nullify(fake_pkernelSIC%kernel)

    !nullify the G%rxyz pointer
    nullify(G%rxyz)
    !extract the gaussian basis from the pseudowavefunctions
    call gaussian_pswf_basis(21,.false.,iproc,nspin,at,rxyz,G,gbd_occ)

    gaucoeffs = f_malloc((/ G%ncoeff, orbs%norbp*orbs%nspinor /),id='gaucoeffs')

    !fill randomly the gaussian coefficients for the orbitals considered
    do iorb=1,orbs%norbp*orbs%nspinor
       do icoeff=1,G%ncoeff
          call random_number(tt)
          gaucoeffs(icoeff,iorb)=real(tt,wp)
       end do
    end do

    !allocate the wavefunctions
    psi = f_malloc0((/ array_dim(Lzd%Glr), orbs%nspinor*orbs%norbp /),id='psi')
    hpsi = f_malloc0((/ array_dim(Lzd%Glr) , orbs%nspinor*orbs%norbp /),id='hpsi')

    !convert the gaussians in wavelets
    call gaussians_to_wavelets_new(iproc,nproc,Lzd,orbs,G,gaucoeffs,psi)

    call f_free(gaucoeffs)
    call f_free_ptr(gbd_occ)

    !deallocate the gaussian basis descriptors
    call deallocate_gwf(G)

    if (ixc < 0) then
       call xc_init(xc, ixc, XC_MIXED, nspin)
    else
       call xc_init(xc, ixc, XC_ABINIT, nspin)
    end if

    !allocate and initialise the potential and the density
    pot = f_malloc((/ Lzd%Glr%mesh%ndims(1), Lzd%Glr%mesh%ndims(2), Lzd%Glr%mesh%ndims(3), nspin /),id='pot')
    rho = f_malloc((/ Lzd%Glr%mesh%ndims(1), Lzd%Glr%mesh%ndims(2), Lzd%Glr%mesh%ndims(3), nspin /),id='rho')

    !here the potential can be used for building the density
    call dpbox_set(dpbox, Lzd%Glr%mesh, xc, iproc, nproc, bigdft_mpi%mpi_comm, &
         "NONE", nspin)

    !components of the charge density
    if (orbs%nspinor ==4) then
       nspinn=4
    else
       nspinn=nspin
    end if

    !flag for toggling the REDUCE_SCATTER stategy
    rsflag = .not.xc_isgga(xc)

    !calculate dimensions of the complete array to be allocated before the reduction procedure
    if (rsflag) then
       nrhotot=0
       do jproc=0,nproc-1
          nrhotot=nrhotot+dpbox%nscatterarr(jproc,1)
       end do
    else
       nrhotot=Lzd%Glr%mesh%ndims(3)
    end if

    !allocate the necessary objects on the GPU
    !set initialisation of GPU part
    !initialise the acceleration strategy if required
    call init_material_acceleration(iproc,matacc,GPU)

    !allocate arrays for the GPU if a card is present
    if (GPU%OCLconv) then
       !the same with OpenCL, but they cannot exist at same time
       call allocate_data_OCL(Lzd%Glr,nspin,orbs,GPU)
    end if
    if (iproc == 0) write(*,*)&
         &   'GPU data allocated'

    write(*,'(1x,a)')repeat('-',34)//' CPU-GPU comparison: Density calculation'

    !for each of the orbitals treated by the processor build the partial densities
    !call cpu_time(t0)
    call nanosec(itsc0)
    do j=1,ntimes
       call tenminustwenty(Lzd%Glr%mesh%ndims(1)*Lzd%Glr%mesh%ndims(2)*nrhotot*nspinn,pot,nproc)
       call local_partial_density(nproc,rsflag,dpbox%nscatterarr,&
            nrhotot,Lzd%Glr,nspin,orbs,psi,pot,.false.,pot,orbs%occup)
    end do
    call nanosec(itsc1)
    !call cpu_time(t1)
    !CPUtime=real(t1-t0,kind=8)
    CPUtime=real(itsc1-itsc0,kind=8)*1.d-9

    !now the GPU part
    !for each of the orbitals treated by the processor build the partial densities
    !call cpu_time(t0)
    call nanosec(itsc0)
    do j=1,ntimes
       !switch between GPU/CPU treatment of the density
       if (GPU%OCLconv) then
          call local_partial_density_OCL(orbs,nrhotot,Lzd%Glr,0.5_gp*hx,0.5_gp*hy,0.5_gp*hz,nspin,psi,rho,GPU)
       end if
    end do
    call nanosec(itsc1)
    !call cpu_time(t1)
    !GPUtime=real(t1-t0,kind=8)
    GPUtime=real(itsc1-itsc0,kind=8)*1.d-9

    call dpbox_free(dpbox)


    !compare the results between the different actions of the hamiltonian
    !check the differences between the results
    call compare_data_and_gflops(CPUtime,GPUtime,&
         & real(Lzd%Glr%mesh%ndim,kind=8)*192.d0,pot,rho,&
         int(Lzd%Glr%mesh%ndim),ntimes*orbs%norbp,.false.,Rden)

    call f_free(rho)


    !here the grid spacings are the small ones
    sigma2=0.125_gp*((Lzd%Glr%mesh%ndims(1)*hx)**2+(Lzd%Glr%mesh%ndims(2)*hy)**2+(Lzd%Glr%mesh%ndims(3)*hz)**2)
    do ispin=1,nspin
       do i3=1,Lzd%Glr%mesh%ndims(3)
          z=hz*real(i3-Lzd%Glr%mesh%ndims(3)/2-1,gp)
          do i2=1,Lzd%Glr%mesh%ndims(2)
             y=hy*real(i2-Lzd%Glr%mesh%ndims(2)/2-1,gp)
             do i1=1,Lzd%Glr%mesh%ndims(1)
                x=hx*real(i1-Lzd%Glr%mesh%ndims(1)/2-1,gp)
                !tt=abs(dsin(real(i1+i2+i3,kind=8)+.7d0))
                r2=x**2+y**2+z**2
                arg=0.5d0*r2/sigma2
                ttd=dexp(-arg)

                pot(i1,i2,i3,ispin)=ttd
             end do
          end do
       end do
    end do

    write(*,'(1x,a)')repeat('-',34)//' CPU-GPU comparison: Local Hamiltonian calculation'

    !warm-up
    !call local_hamiltonian(iproc,orbs,Lzd%Glr,hx,hy,hz,nspin,pot,psi,hpsi,ekin_sum,epot_sum)

    !apply the CPU hamiltonian
    !take timings
    call nanosec(itsc0)
    xc%ixc = 0
    do j=1,ntimes
       pottmp = f_malloc_ptr(Lzd%Glr%mesh%ndim*nspin,id='pottmp')
       call vcopy(int(Lzd%Glr%mesh%ndim)*nspin,pot(1,1,1,1),1,pottmp(1),1)
       call local_hamiltonian_old(iproc,nproc,orbs%npsidim_orbs,orbs,Lzd,hx,hy,hz,0,confdatarr,pottmp,psi,hpsi, &
            fake_pkernelSIC,xc,0.0_gp,ekin_sum,epot_sum,eSIC_DC)
       call f_free_ptr(pottmp)
    end do
    xc%ixc = ixc
    call nanosec(itsc1)
    CPUtime=real(itsc1-itsc0,kind=8)*1.d-9

    print *,'ekin,epot=',ekin_sum,epot_sum

    !WARNING: local hamiltonian overwrites the psis
    !warm-up
    !call gpu_locham(Lzd%Glr%d%n1,Lzd%Glr%d%n2,Lzd%Glr%d%n3,hx,hy,hz,orbs,GPU,ekinGPU,epotGPU)

    !apply the GPU hamiltonian and put the results in the hpsi_GPU array
    GPU%hpsi_ASYNC = f_malloc_ptr(array_dim(Lzd%Glr)*orbs%nspinor*orbs%norbp,id='GPU%hpsi_ASYNC')

    !take timings
    call nanosec(itsc0)
    do j=1,ntimes
       if (GPU%OCLconv) then
          call local_hamiltonian_OCL(orbs,Lzd%Glr,orbs%nspin,pot,psi,GPU%hpsi_ASYNC,ekinGPU,epotGPU,GPU)
       end if
    end do
    if(ASYNCconv .and. GPU%OCLconv) call finish_hamiltonian_OCL(orbs,ekinGPU,epotGPU,GPU)
    call nanosec(itsc1)
    GPUtime=real(itsc1-itsc0,kind=8)*1.d-9

    print *,'ekinGPU,epotGPU',ekinGPU,epotGPU

    !compare the results between the different actions of the hamiltonian
    !check the differences between the results
    call compare_data_and_gflops(CPUtime,GPUtime,&
         &   real(Lzd%Glr%mesh%ndim,kind=8)*real(192+46*3+192+2,kind=8),hpsi,GPU%hpsi_ASYNC,&
         orbs%norbp*orbs%nspinor*int(array_dim(Lzd%Glr)),ntimes*orbs%norbp,.false.,Rham)

    call f_free(pot)

    write(*,'(1x,a)')repeat('-',34)//' CPU-GPU comparison: Linear Algebra (Blas)'

    !perform the scalar product between the hpsi wavefunctions
    !actually this is <hpsi|hpsi> it has no meaning.
    !this works only if nspinor==1
    overlap = f_malloc((/ orbs%norbp, orbs%norbp, 2 /),id='overlap')

    call nanosec(itsc0)
    do j=1,ntimes
       call DGEMM('T','N',orbs%norbp,orbs%norbp,array_dim(Lzd%Glr),1.0_wp,&
            &   psi(1,1),array_dim(Lzd%Glr),&
            hpsi(1,1),array_dim(Lzd%Glr),0.0_wp,&
            &   overlap(1,1,1),orbs%norbp)
    end do
    call nanosec(itsc1)
    CPUtime=real(itsc1-itsc0,kind=8)*1.d-9


    call nanosec(itsc0)
    do j=1,ntimes
       call GEMMSY('T','N',orbs%norbp,orbs%norbp,array_dim(Lzd%Glr),1.0_wp,&
            &   psi(1,1),array_dim(Lzd%Glr),&
            hpsi(1,1),array_dim(Lzd%Glr),0.0_wp,&
            &   overlap(1,1,2),orbs%norbp)
    end do
    call nanosec(itsc1)
    GPUtime=real(itsc1-itsc0,kind=8)*1.d-9

    !comparison between the results
    call compare_data_and_gflops(CPUtime,GPUtime,&
         &   real(orbs%norbp**2,kind=8)*real(array_dim(Lzd%Glr)*2,kind=8),overlap(1,1,1),overlap(1,1,2),&
         orbs%norbp**2,ntimes,.false.,Rgemm)


    nvctrp=array_dim(Lzd%Glr)

    call nanosec(itsc0)
    do j=1,ntimes
       call dsyrk('L','T',orbs%norbp,nvctrp,1.0_wp,psi(1,1),nvctrp,0.0_wp,&
            &   overlap(1,1,1),orbs%norbp)
    end do
    call nanosec(itsc1)
    CPUtime=real(itsc1-itsc0,kind=8)*1.d-9


    call nanosec(itsc0)
    do j=1,ntimes
       call syrk('L','T',orbs%norbp,nvctrp,1.0_wp,psi(1,1),nvctrp,0.0_wp,&
            &   overlap(1,1,2),orbs%norbp)
    end do
    call nanosec(itsc1)
    GPUtime=real(itsc1-itsc0,kind=8)*1.d-9

    call compare_data_and_gflops(CPUtime,GPUtime,&
         real(orbs%norbp*(orbs%norbp+1),kind=8)*&
         real(array_dim(Lzd%Glr),kind=8),overlap(1,1,1),overlap(1,1,2),&
         orbs%norbp**2,ntimes,.false.,Rsyrk)

    call f_free(overlap)


    !-------------------now the same for preconditioning
    write(*,'(1x,a)')repeat('-',34)//' CPU-GPU comparison: Preconditioner'

    !the input function is psi
    call nanosec(itsc0)
    do j=1,ntimes
       call preconditionall(orbs,Lzd%Glr,hx,hy,hz,ncong,hpsi,gnrm,gnrm_zero)
    end do
    call nanosec(itsc1)

    CPUtime=real(itsc1-itsc0,kind=8)*1.d-9
    print *,'gnrm',gnrm


    !GPU data are aLzd%Glready on the card, must be only copied back
    !the input function is GPU%hpsi in that case
    call nanosec(itsc0)
    do j=1,ntimes
       !Preconditions all orbitals belonging to iproc
       !and calculate the partial norm of the residue
       !switch between CPU and GPU treatment
       if (GPU%OCLconv) then
          call preconditionall_OCL(orbs,Lzd%Glr,ncong,&
               &   GPU%hpsi_ASYNC,gnrmGPU,gnrm_zero,GPU)
       end if
    end do
    call nanosec(itsc1)

    GPUtime=real(itsc1-itsc0,kind=8)*1.d-9
    print *,'gnrmGPU',gnrmGPU

    call compare_data_and_gflops(CPUtime,GPUtime,&
         &   real(Lzd%Glr%mesh%ndim,kind=8)*real((192+46*3+192+2-1+12)*(ncong+1),kind=8),hpsi,GPU%hpsi_ASYNC,&
         orbs%norbp*orbs%nspinor*array_dim(Lzd%Glr),ntimes*orbs%norbp,.false.,Rprec)

    call f_free_ptr(GPU%hpsi_ASYNC)
    call f_free(psi)
    call f_free(hpsi)


    !free the card at the end
    if (GPU%OCLconv) then
       call free_gpu_OCL(GPU,orbs,nspin)
    end if

    call xc_end(xc)

    !finalise the material accelearion usage
    call release_material_acceleration(GPU)


    write(*,'(1x,a,5(1x,f7.3))')'Ratios:',Rden,Rham,Rgemm,Rsyrk,Rprec

  END SUBROUTINE compare_cpu_gpu_hamiltonian


  subroutine compare_data_and_gflops(CPUtime,GPUtime,GFlopsfactor,&
       &   CPUdata,GPUdata,n,ntimes,dowrite,ratio)
    use module_precisions
    implicit none
    logical, intent(in) :: dowrite
    integer, intent(in) :: n,ntimes
    real(gp), intent(in) :: CPUtime,GPUtime,GFlopsfactor
    real(gp), intent(out) :: ratio
    real(wp), dimension(n), intent(in) :: CPUdata,GPUdata
    !local variables
    integer :: i
    real(gp) :: CPUGflops,GPUGflops,maxdiff,comp,threshold

    threshold=1.d-12
    !un-initialize valies which might suffer from fpe
    GPUGflops=-1.0_gp
    CPUGflops=-1.0_gp
    ratio=-1.0_gp

    if (CPUtime > 0.0_gp) CPUGflops=GFlopsfactor*real(ntimes,gp)/(CPUtime*1.d9)
    if (GPUtime > 0.0_gp) GPUGflops=GFlopsfactor*real(ntimes,gp)/(GPUtime*1.d9)

    maxdiff=0.0_gp

    rewind(17)

    do i=1,n
       if (dowrite) write(17,'(i6,2(1pe24.17))')i,CPUdata(i),GPUdata(i)
       comp=abs(CPUdata(i)-GPUdata(i))
       maxdiff=max(maxdiff,comp)
    end do
    if (GPUtime > 0.0_gp) ratio=CPUtime/GPUtime
    write(*,'(1x,a)')'| CPU: ms  |  Gflops  || GPU:  ms |  GFlops  || Ratio  | No. Elements | Max. Diff. |'

    write(*,'(1x,2(2(a,f10.2),a),a,f8.3,a,i14,a,1pe12.4,a)',advance='no')&
         &   '|',CPUtime*1.d3/real(ntimes,kind=8),'|',& ! Time CPU (ms)
         &   CPUGflops,'|',& !Gflops CPU (ms)
         &   '|',GPUtime*1.d3/real(ntimes,kind=8),'|',& ! Time GPU (ms)
         &   GPUGflops,'|',&!Gflops GPU (ms)
         &   '|',ratio,'|',& ! ratio
         &   n,'|',& !No. elements
         &   maxdiff,'|' ! maxdiff
    if (maxdiff <= threshold) then
       write(*,'(a)')''
    else
       write(*,'(a)')'<<<< WARNING'
    end if

  END SUBROUTINE compare_data_and_gflops

  subroutine memoryGuess(radical, nproc, logfile, outputgrid)
    use module_precisions
    use dictionaries
    use bigdft_run
    use module_bigdft_arrays
    use at_domain
    use module_atoms
    use module_types
    use module_fragments
    use public_enums
    use module_input_keys
    use f_enums
    use module_interfaces
    use locregs
    use yaml_output
    use communications_base, only: deallocate_comms
    use psp_projectors_base, only: free_DFT_PSP_projectors
    use io
    implicit none
    character(len = *), intent(in) :: radical
    integer, intent(in) :: nproc
    logical, intent(in) :: logfile, outputgrid

    type(dictionary), pointer :: run
    type(run_objects) :: runObj
    integer :: i, input_wf_format, ierror
    type(system_fragment), dimension(:), pointer :: ref_frags
    type(DFT_PSP_projectors) :: nlpsp
    type(f_enumerator) :: inputpsi
    type(memory_estimation) :: mem

    nullify(run)
    call bigdft_set_run_properties(run, run_id = trim(radical), &
         run_from_files = .true., log_to_disk = logfile, minimal_file=trim(radical))

    call run_objects_init(runObj, run)
    call dict_free(run)

    call print_dft_parameters(runObj%inputs,runObj%atoms)

    ! Run memory estimation on sections, if DFT mode.
    if (associated(runObj%sections)) then
       do i = 1, size(runObj%sections)
          if (runObj%sections(i)%run_mode == QM_RUN_MODE) then
             inputpsi = runObj%sections(i)%inputs%inputPsiId
             call system_initialization(0, nproc, .true.,inputpsi, input_wf_format, .true., &
                  & runObj%sections(i)%inputs, runObj%sections(i)%atoms, &
                  & runObj%sections(i)%atoms%astruct%rxyz, runObj%sections(i)%rst%GPU%OCLconv, &
                  & runObj%sections(i)%rst%KSwfn%orbs, runObj%sections(i)%rst%tmb%npsidim_orbs, &
                  & runObj%sections(i)%rst%tmb%npsidim_comp, runObj%sections(i)%rst%tmb%orbs, &
                  & runObj%sections(i)%rst%KSwfn%Lzd, runObj%sections(i)%rst%tmb%Lzd, nlpsp, &
                  & runObj%sections(i)%rst%KSwfn%comms, ref_frags, &
                  & output_grid = outputgrid)
             call MemoryEstimator(nproc,runObj%sections(i)%inputs%idsx, &
                  & runObj%sections(i)%rst%KSwfn%Lzd%Glr, runObj%sections(i)%rst%KSwfn%orbs%norb, &
                  & runObj%sections(i)%rst%KSwfn%orbs%nspinor, runObj%sections(i)%rst%KSwfn%orbs%nkpts, &
                  & nlpsp%nprojel, runObj%sections(i)%inputs%nspin, &
                  & runObj%sections(i)%inputs%itrpmax,toi(runObj%sections(i)%inputs%scf),mem)
             call free_DFT_PSP_projectors(nlpsp)
             call deallocate_Lzd_except_Glr(runObj%sections(i)%rst%KSwfn%Lzd)
             call deallocate_comms(runObj%sections(i)%rst%KSwfn%comms)
             call deallocate_orbs(runObj%sections(i)%rst%KSwfn%orbs)

             call print_memory_estimation(mem)

             call astruct_dump_to_file(runObj%sections(i)%atoms%astruct,&
                  & trim(runObj%sections(i)%inputs%dir_output)//trim(runObj%sections(i)%label),&
                  & "extracted for section " // trim(runObj%sections(i)%label))
          end if
       end do
    end if

    inputpsi = runObj%inputs%inputPsiId
    call system_initialization(0, nproc, .true.,inputpsi, input_wf_format, .true., &
         & runObj%inputs, runObj%atoms, runObj%atoms%astruct%rxyz, runObj%rst%GPU%OCLconv, &
         & runObj%rst%KSwfn%orbs, runObj%rst%tmb%npsidim_orbs, runObj%rst%tmb%npsidim_comp, &
         & runObj%rst%tmb%orbs, runObj%rst%KSwfn%Lzd, runObj%rst%tmb%Lzd, nlpsp, runObj%rst%KSwfn%comms, &
         & ref_frags, output_grid = outputgrid)

    call MemoryEstimator(nproc,runObj%inputs%idsx,runObj%rst%KSwfn%Lzd%Glr,&
         & runObj%rst%KSwfn%orbs%norb,runObj%rst%KSwfn%orbs%nspinor,&
         & runObj%rst%KSwfn%orbs%nkpts,nlpsp%nprojel,&
         runObj%inputs%nspin,runObj%inputs%itrpmax,toi(runObj%inputs%scf),mem)

    if (runObj%run_mode /= MULTI_RUN_MODE .and. &
         & .not.(inputpsi .hasattr. 'LINEAR')) then
       call print_memory_estimation(mem)
    end if

    ! De-allocations
    call deallocate_Lzd_except_Glr(runObj%rst%KSwfn%Lzd)
    call deallocate_comms(runObj%rst%KSwfn%comms)
    call deallocate_orbs(runObj%rst%KSwfn%orbs)
    call free_DFT_PSP_projectors(nlpsp)

    !remove the directory which has been created if it is possible
    call deldir(runObj%inputs%dir_output,len(trim(runObj%inputs%dir_output)),ierror)

    call free_run_objects(runObj)
    !   !finalize memory counting
    !   call memocc(0,0,'count','stop')
  end subroutine memoryGuess
end module tools
