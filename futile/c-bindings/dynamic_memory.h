#ifndef DYNAMIC_MEMORY_H
#define DYNAMIC_MEMORY_H

#include "futile_cst.h"

F_DEFINE_TYPE(int_1d);
typedef struct {
    f90_int_1d_pointer ptr;
    size_t len;
} int_1d_pointer;
void int_1d_pointer_free(int_1d_pointer *ptr);

F_DEFINE_TYPE(double_2d);
typedef struct {
    f90_double_2d_pointer ptr;
    size_t len[2];
} double_2d_pointer;
void double_2d_pointer_free(double_2d_pointer *ptr);

#endif
