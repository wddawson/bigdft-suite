#include "DynamicMemory"
#include "config.h"

using namespace Futile;

extern "C" {
    void FC_FUNC(f_free_ptr_i1, F_FREE_PTR_I1)(Int1dPointer::f90_int_1d_pointer*);
    void FC_FUNC(f_free_ptr_d2, F_FREE_PTR_D2)(Double2dPointer::f90_double_2d_pointer*);
}

Int1dPointer::~Int1dPointer()
{
  if (mData) {
    FC_FUNC(f_free_ptr_i1, F_FREE_PTR_I1)(&mPointer);
  }
}

Double2dPointer::~Double2dPointer()
{
  if (mData) {
    FC_FUNC(f_free_ptr_d2, F_FREE_PTR_D2)(&mPointer);
  }
}
