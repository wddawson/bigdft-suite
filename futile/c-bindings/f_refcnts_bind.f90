subroutine bind_f90_f_reference_counter_copy_constructor( &
    out_self, &
    other)
  use dictionaries
  use f_refcnts
  implicit none
  type(f_reference_counter), pointer :: out_self
  type(f_reference_counter), intent(in) :: other

  nullify(out_self)
  allocate(out_self)
  out_self = other
end subroutine bind_f90_f_reference_counter_copy_constructor

subroutine bind_f_associated( &
    out_f_associated, &
    ref)
  use dictionaries
  use f_refcnts
  implicit none
  logical :: out_f_associated
  type(f_reference_counter), intent(in) :: ref

  out_f_associated = f_associated( &
    ref)
end subroutine bind_f_associated

subroutine bind_f_ref_null( &
    out_ref)
  use dictionaries
  use f_refcnts
  implicit none
  type(f_reference_counter), pointer :: out_ref

  nullify(out_ref)
  allocate(out_ref)
  out_ref = f_ref_null( &
)
end subroutine bind_f_ref_null

subroutine bind_f_ref_new( &
    out_ref, &
    id, &
    id_len, &
    address)
  use dictionaries
  use f_precisions
  use f_refcnts
  implicit none
  type(f_reference_counter), pointer :: out_ref
  integer(kind = f_long), intent(in) :: id_len
  integer(kind = f_address), optional, intent(in) :: address
  character(len = id_len), intent(in) :: id

  nullify(out_ref)
  allocate(out_ref)
  out_ref = f_ref_new( &
    id, &
    address)
end subroutine bind_f_ref_new

subroutine bind_f_ref_count( &
    out_count, &
    ref)
  use dictionaries
  use f_refcnts
  implicit none
  integer :: out_count
  type(f_reference_counter), intent(in) :: ref

  out_count = f_ref_count( &
    ref)
end subroutine bind_f_ref_count

subroutine bind_refcnts_errors( &
    )
  use dictionaries
  use f_refcnts
  implicit none

  call refcnts_errors( &
)
end subroutine bind_refcnts_errors

subroutine bind_nullify_f_ref( &
    ref)
  use dictionaries
  use f_refcnts
  implicit none
  type(f_reference_counter), intent(out) :: ref

  call nullify_f_ref( &
    ref)
end subroutine bind_nullify_f_ref

subroutine bind_f_unref( &
    ref, &
    count)
  use dictionaries
  use f_refcnts
  implicit none
  type(f_reference_counter), intent(inout) :: ref
  integer, optional, intent(out) :: count

  call f_unref( &
    ref, &
    count)
end subroutine bind_f_unref

subroutine bind_f_ref_free( &
    ref)
  use dictionaries
  use f_refcnts
  implicit none
  type(f_reference_counter), intent(inout) :: ref

  call f_ref_free( &
    ref)
end subroutine bind_f_ref_free

subroutine bind_f_ref( &
    src)
  use dictionaries
  use f_refcnts
  implicit none
  type(f_reference_counter), intent(inout) :: src

  call f_ref( &
    src)
end subroutine bind_f_ref

subroutine bind_f_ref_associate( &
    src, &
    dest)
  use yaml_output, only: yaml_dict_dump
  use dictionaries
  use f_refcnts
  implicit none
  type(f_reference_counter), intent(in) :: src
  type(f_reference_counter), intent(inout) :: dest

  call f_ref_associate( &
    src, &
    dest)
end subroutine bind_f_ref_associate

