#include "Misc"
#include "config.h"

extern "C" {
void FC_FUNC_(f_lib_initialize, F_LIB_INITIALIZE)(void);
void FC_FUNC_(f_lib_finalize, F_LIB_FINALIZE)(void);
}

void Futile::initialize()
{
  FC_FUNC_(f_lib_initialize, F_LIB_INITIALIZE)();
}
void Futile::finalize()
{
  FC_FUNC_(f_lib_finalize, F_LIB_FINALIZE)();
}
