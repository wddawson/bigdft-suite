module f_zero_module

   use f_precisions

   implicit none

   private

   integer, public, save :: TCAT_INIT_TO_ZERO

! .. f:function:: f_zero(x [, size, length])

!     Initializes to zero the values of x. The variable x can be of any intrinsic fortran type, kind and rank.
!     Zero values for logical and character types are assumed as .false. and blanks, respectively.
!     If size is present, the variable x must be scalar, and intialization is performed for all memory elements
!     which follow the x address in memory, following the kint and type convention of x.
!     If x if of type character and of rank larger than one, or if size is specified, an extra argument len has to be specified indicating
!     the length of each of the element of the array.
!     If x is an array, is must be allocated on entry. Arrays of size larger than 1024 elements are initialized
!     with multithreaded parallelization if the functon is not called from whithin a parallel region.

   interface f_zero
      module procedure f_zero_r0
      module procedure f_zero_d0
      ! module procedure f_zero_q0
      module procedure f_zero_s0
      module procedure f_zero_i0
      module procedure f_zero_li0
      module procedure f_zero_b0
      module procedure f_zero_l0
      module procedure f_zero_a0
      module procedure f_zero_c0
      module procedure f_zero_z0
      module procedure f_zero_r1
      module procedure f_zero_r2
      module procedure f_zero_r3
      module procedure f_zero_r4
      module procedure f_zero_r5
      module procedure f_zero_r6
      module procedure f_zero_r7
      module procedure f_zero_d1
      module procedure f_zero_d2
      module procedure f_zero_d3
      module procedure f_zero_d4
      module procedure f_zero_d5
      module procedure f_zero_d6
      module procedure f_zero_d7
      ! module procedure f_zero_q1
      ! module procedure f_zero_q2
      ! module procedure f_zero_q3
      ! module procedure f_zero_q4
      ! module procedure f_zero_q5
      ! module procedure f_zero_q6
      ! module procedure f_zero_q7
      ! module procedure f_zero_s1
      module procedure f_zero_s2
      module procedure f_zero_s3
      module procedure f_zero_s4
      module procedure f_zero_s5
      module procedure f_zero_s6
      module procedure f_zero_s7
      module procedure f_zero_i1
      module procedure f_zero_i2
      module procedure f_zero_i3
      module procedure f_zero_i4
      module procedure f_zero_i5
      module procedure f_zero_i6
      module procedure f_zero_i7
      module procedure f_zero_li1
      module procedure f_zero_li2
      module procedure f_zero_li3
      module procedure f_zero_li4
      module procedure f_zero_li5
      module procedure f_zero_li6
      module procedure f_zero_li7
      module procedure f_zero_b1
      module procedure f_zero_b2
      module procedure f_zero_b3
      module procedure f_zero_b4
      module procedure f_zero_b5
      module procedure f_zero_b6
      module procedure f_zero_b7
      module procedure f_zero_l1
      module procedure f_zero_l2
      module procedure f_zero_l3
      module procedure f_zero_l4
      module procedure f_zero_l5
      module procedure f_zero_l6
      module procedure f_zero_l7
      module procedure f_zero_a1
      module procedure f_zero_a2
      module procedure f_zero_a3
      module procedure f_zero_a4
      module procedure f_zero_a5
      module procedure f_zero_a6
      module procedure f_zero_a7
      module procedure f_zero_c1
      module procedure f_zero_c2
      module procedure f_zero_c3
      module procedure f_zero_c4
      module procedure f_zero_c5
      module procedure f_zero_c6
      module procedure f_zero_c7
      module procedure f_zero_z1
      module procedure f_zero_z2
      module procedure f_zero_z3
      module procedure f_zero_z4
      module procedure f_zero_z5
      module procedure f_zero_z6
      module procedure f_zero_z7
      module procedure f_zero_i_r0
      module procedure f_zero_li_r0
      module procedure f_zero_i_d0
      module procedure f_zero_li_d0
      ! module procedure f_zero_i_q0
      ! module procedure f_zero_li_q0
      module procedure f_zero_i_s0
      module procedure f_zero_li_s0
      module procedure f_zero_i_i0
      module procedure f_zero_li_i0
      module procedure f_zero_i_li0
      module procedure f_zero_li_li0
      module procedure f_zero_i_b0
      module procedure f_zero_li_b0
      module procedure f_zero_i_l0
      module procedure f_zero_li_l0
      module procedure f_zero_i_a0
      module procedure f_zero_li_a0
      module procedure f_zero_i_c0
      module procedure f_zero_li_c0
      module procedure f_zero_i_z0
      module procedure f_zero_li_z0
   end interface f_zero

public :: f_zero

contains

pure subroutine f_zero_r0(array)
   real(kind=f_simple), intent(out) :: array
   array = real(0,kind=f_simple)
end subroutine f_zero_r0

pure subroutine f_zero_d0(array)
   real(kind=f_double), intent(out) :: array
   array = real(0,kind=f_double)
end subroutine f_zero_d0

! pure subroutine f_zero_q0(array)
!    real(kind=f_quadruple), intent(out) :: array
!    array = real(0,kind=f_quadruple)
! end subroutine f_zero_q0

pure subroutine f_zero_s0(array)
   integer(kind=f_short), intent(out) :: array
   array = int(0,kind=f_short)
end subroutine f_zero_s0

pure subroutine f_zero_i0(array)
   integer(kind=f_integer), intent(out) :: array
   array = int(0,kind=f_integer)
end subroutine f_zero_i0

pure subroutine f_zero_li0(array)
   integer(kind=f_long), intent(out) :: array
   array = int(0,kind=f_long)
end subroutine f_zero_li0

pure subroutine f_zero_b0(array)
   logical(kind=f_byte), intent(out) :: array
   array = logical(.false.,kind=f_byte)
end subroutine f_zero_b0

pure subroutine f_zero_l0(array)
   logical, intent(out) :: array
   array = logical(.false.)
end subroutine f_zero_l0

pure subroutine f_zero_a0(array)
   character(len=*), intent(out) :: array
   array = ' '
end subroutine f_zero_a0

pure subroutine f_zero_c0(array)
   complex(kind=f_simple), intent(out) :: array
   array = cmplx(0,kind=f_simple)
end subroutine f_zero_c0

pure subroutine f_zero_z0(array)
   complex(kind=f_double), intent(out) :: array
   array = cmplx(0,kind=f_double)
end subroutine f_zero_z0

subroutine f_zero_i_r0(array, size)
   integer(kind=f_integer), intent(in) :: size
   real(kind=f_simple) :: array
   external :: setzero_r1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_r1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_i_r0

subroutine f_zero_li_r0(array, size)
   integer(kind=f_long), intent(in) :: size
   real(kind=f_simple) :: array
   external :: setzero_r1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_r1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_li_r0

subroutine f_zero_i_d0(array, size)
   integer(kind=f_integer), intent(in) :: size
   real(kind=f_double) :: array
   external :: setzero_d1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_d1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_i_d0

subroutine f_zero_li_d0(array, size)
   integer(kind=f_long), intent(in) :: size
   real(kind=f_double) :: array
   external :: setzero_d1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_d1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_li_d0

! subroutine f_zero_i_q0(array, size)
!    integer(kind=f_integer), intent(in) :: size
!    real(kind=f_quadruple) :: array
!    external :: setzero_q1
!    call f_timer_interrupt(TCAT_INIT_TO_ZERO)
!    call setzero_q1(array, int(size,kind=f_long))
!    call f_timer_resume()
! end subroutine f_zero_i_q0

! subroutine f_zero_li_q0(array, size)
!    integer(kind=f_long), intent(in) :: size
!    real(kind=f_quadruple) :: array
!    external :: setzero_q1
!    call f_timer_interrupt(TCAT_INIT_TO_ZERO)
!    call setzero_q1(array, int(size,kind=f_long))
!    call f_timer_resume()
! end subroutine f_zero_li_q0

subroutine f_zero_i_s0(array, size)
   integer(kind=f_integer), intent(in) :: size
   integer(kind=f_short) :: array
   external :: setzero_s1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_s1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_i_s0

subroutine f_zero_li_s0(array, size)
   integer(kind=f_long), intent(in) :: size
   integer(kind=f_short) :: array
   external :: setzero_s1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_s1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_li_s0

subroutine f_zero_i_i0(array, size)
   integer(kind=f_integer), intent(in) :: size
   integer(kind=f_integer) :: array
   external :: setzero_i1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_i1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_i_i0

subroutine f_zero_li_i0(array, size)
   integer(kind=f_long), intent(in) :: size
   integer(kind=f_integer) :: array
   external :: setzero_i1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_i1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_li_i0

subroutine f_zero_i_li0(array, size)
   integer(kind=f_integer), intent(in) :: size
   integer(kind=f_long) :: array
   external :: setzero_li1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_li1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_i_li0

subroutine f_zero_li_li0(array, size)
   integer(kind=f_long), intent(in) :: size
   integer(kind=f_long) :: array
   external :: setzero_li1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_li1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_li_li0

subroutine f_zero_i_b0(array, size)
   integer(kind=f_integer), intent(in) :: size
   logical(kind=f_byte) :: array
   external :: setzero_b1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_b1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_i_b0

subroutine f_zero_li_b0(array, size)
   integer(kind=f_long), intent(in) :: size
   logical(kind=f_byte) :: array
   external :: setzero_b1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_b1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_li_b0

subroutine f_zero_i_l0(array, size)
   integer(kind=f_integer), intent(in) :: size
   logical :: array
   external :: setzero_l1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_l1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_i_l0

subroutine f_zero_li_l0(array, size)
   integer(kind=f_long), intent(in) :: size
   logical :: array
   external :: setzero_l1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_l1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_li_l0

subroutine f_zero_i_a0(array, size)
   integer(kind=f_integer), intent(in) :: size
   character(len=*) :: array
   external :: setzero_a1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_a1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_i_a0

subroutine f_zero_li_a0(array, size)
   integer(kind=f_long), intent(in) :: size
   character(len=*) :: array
   external :: setzero_a1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_a1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_li_a0

subroutine f_zero_i_c0(array, size)
   integer(kind=f_integer), intent(in) :: size
   complex(kind=f_simple) :: array
   external :: setzero_c1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_c1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_i_c0

subroutine f_zero_li_c0(array, size)
   integer(kind=f_long), intent(in) :: size
   complex(kind=f_simple) :: array
   external :: setzero_c1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_c1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_li_c0

subroutine f_zero_i_z0(array, size)
   integer(kind=f_integer), intent(in) :: size
   complex(kind=f_double) :: array
   external :: setzero_z1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_z1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_i_z0

subroutine f_zero_li_z0(array, size)
   integer(kind=f_long), intent(in) :: size
   complex(kind=f_double) :: array
   external :: setzero_z1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_z1(array, int(size,kind=f_long))
   call f_timer_resume()
end subroutine f_zero_li_z0

subroutine f_zero_r1(array)
   implicit none
   real(kind=f_simple), dimension(:), intent(out) :: array
   external :: setzero_r1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_r1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_r1

subroutine f_zero_r2(array)
   implicit none
   real(kind=f_simple), dimension(:,:), intent(out) :: array
   external :: setzero_r1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_r1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_r2

subroutine f_zero_r3(array)
   implicit none
   real(kind=f_simple), dimension(:,:,:), intent(out) :: array
   external :: setzero_r1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_r1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_r3

subroutine f_zero_r4(array)
   implicit none
   real(kind=f_simple), dimension(:,:,:,:), intent(out) :: array
   external :: setzero_r1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_r1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_r4

subroutine f_zero_r5(array)
   implicit none
   real(kind=f_simple), dimension(:,:,:,:,:), intent(out) :: array
   external :: setzero_r1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_r1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_r5

subroutine f_zero_r6(array)
   implicit none
   real(kind=f_simple), dimension(:,:,:,:,:,:), intent(out) :: array
   external :: setzero_r1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_r1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_r6

subroutine f_zero_r7(array)
   implicit none
   real(kind=f_simple), dimension(:,:,:,:,:,:,:), intent(out) :: array
   external :: setzero_r1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_r1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_r7

subroutine f_zero_d1(array)
   implicit none
   real(kind=f_double), dimension(:), intent(out) :: array
   external :: setzero_d1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_d1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_d1

subroutine f_zero_d2(array)
   implicit none
   real(kind=f_double), dimension(:,:), intent(out) :: array
   external :: setzero_d1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_d1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_d2

subroutine f_zero_d3(array)
   implicit none
   real(kind=f_double), dimension(:,:,:), intent(out) :: array
   external :: setzero_d1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_d1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_d3

subroutine f_zero_d4(array)
   implicit none
   real(kind=f_double), dimension(:,:,:,:), intent(out) :: array
   external :: setzero_d1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_d1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_d4

subroutine f_zero_d5(array)
   implicit none
   real(kind=f_double), dimension(:,:,:,:,:), intent(out) :: array
   external :: setzero_d1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_d1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_d5

subroutine f_zero_d6(array)
   implicit none
   real(kind=f_double), dimension(:,:,:,:,:,:), intent(out) :: array
   external :: setzero_d1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_d1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_d6

subroutine f_zero_d7(array)
   implicit none
   real(kind=f_double), dimension(:,:,:,:,:,:,:), intent(out) :: array
   external :: setzero_d1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_d1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_d7

! subroutine f_zero_q1(array)
!    implicit none
!    real(kind=f_quadruple), dimension(:), intent(out) :: array
!    external :: setzero_q1
!    call f_timer_interrupt(TCAT_INIT_TO_ZERO)
!    call setzero_q1(array, product(int(shape(array),kind=f_long)))
!    call f_timer_resume()
! end subroutine f_zero_q1

! subroutine f_zero_q2(array)
!    implicit none
!    real(kind=f_quadruple), dimension(:,:), intent(out) :: array
!    external :: setzero_q1
!    call f_timer_interrupt(TCAT_INIT_TO_ZERO)
!    call setzero_q1(array, product(int(shape(array),kind=f_long)))
!    call f_timer_resume()
! end subroutine f_zero_q2

! subroutine f_zero_q3(array)
!    implicit none
!    real(kind=f_quadruple), dimension(:,:,:), intent(out) :: array
!    external :: setzero_q1
!    call f_timer_interrupt(TCAT_INIT_TO_ZERO)
!    call setzero_q1(array, product(int(shape(array),kind=f_long)))
!    call f_timer_resume()
! end subroutine f_zero_q3

! subroutine f_zero_q4(array)
!    implicit none
!    real(kind=f_quadruple), dimension(:,:,:,:), intent(out) :: array
!    external :: setzero_q1
!    call f_timer_interrupt(TCAT_INIT_TO_ZERO)
!    call setzero_q1(array, product(int(shape(array),kind=f_long)))
!    call f_timer_resume()
! end subroutine f_zero_q4

! subroutine f_zero_q5(array)
!    implicit none
!    real(kind=f_quadruple), dimension(:,:,:,:,:), intent(out) :: array
!    external :: setzero_q1
!    call f_timer_interrupt(TCAT_INIT_TO_ZERO)
!    call setzero_q1(array, product(int(shape(array),kind=f_long)))
!    call f_timer_resume()
! end subroutine f_zero_q5

! subroutine f_zero_q6(array)
!    implicit none
!    real(kind=f_quadruple), dimension(:,:,:,:,:,:), intent(out) :: array
!    external :: setzero_q1
!    call f_timer_interrupt(TCAT_INIT_TO_ZERO)
!    call setzero_q1(array, product(int(shape(array),kind=f_long)))
!    call f_timer_resume()
! end subroutine f_zero_q6

! subroutine f_zero_q7(array)
!    implicit none
!    real(kind=f_quadruple), dimension(:,:,:,:,:,:,:), intent(out) :: array
!    external :: setzero_q1
!    call f_timer_interrupt(TCAT_INIT_TO_ZERO)
!    call setzero_q1(array, product(int(shape(array),kind=f_long)))
!    call f_timer_resume()
! end subroutine f_zero_q7

subroutine f_zero_s1(array)
   implicit none
   integer(kind=f_short), dimension(:), intent(out) :: array
   external :: setzero_s1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_s1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_s1

subroutine f_zero_s2(array)
   implicit none
   integer(kind=f_short), dimension(:,:), intent(out) :: array
   external :: setzero_s1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_s1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_s2

subroutine f_zero_s3(array)
   implicit none
   integer(kind=f_short), dimension(:,:,:), intent(out) :: array
   external :: setzero_s1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_s1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_s3

subroutine f_zero_s4(array)
   implicit none
   integer(kind=f_short), dimension(:,:,:,:), intent(out) :: array
   external :: setzero_s1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_s1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_s4

subroutine f_zero_s5(array)
   implicit none
   integer(kind=f_short), dimension(:,:,:,:,:), intent(out) :: array
   external :: setzero_s1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_s1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_s5

subroutine f_zero_s6(array)
   implicit none
   integer(kind=f_short), dimension(:,:,:,:,:,:), intent(out) :: array
   external :: setzero_s1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_s1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_s6

subroutine f_zero_s7(array)
   implicit none
   integer(kind=f_short), dimension(:,:,:,:,:,:,:), intent(out) :: array
   external :: setzero_s1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_s1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_s7

subroutine f_zero_i1(array)
   implicit none
   integer(kind=f_integer), dimension(:), intent(out) :: array
   external :: setzero_i1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_i1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_i1

subroutine f_zero_i2(array)
   implicit none
   integer(kind=f_integer), dimension(:,:), intent(out) :: array
   external :: setzero_i1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_i1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_i2

subroutine f_zero_i3(array)
   implicit none
   integer(kind=f_integer), dimension(:,:,:), intent(out) :: array
   external :: setzero_i1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_i1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_i3

subroutine f_zero_i4(array)
   implicit none
   integer(kind=f_integer), dimension(:,:,:,:), intent(out) :: array
   external :: setzero_i1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_i1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_i4

subroutine f_zero_i5(array)
   implicit none
   integer(kind=f_integer), dimension(:,:,:,:,:), intent(out) :: array
   external :: setzero_i1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_i1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_i5

subroutine f_zero_i6(array)
   implicit none
   integer(kind=f_integer), dimension(:,:,:,:,:,:), intent(out) :: array
   external :: setzero_i1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_i1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_i6

subroutine f_zero_i7(array)
   implicit none
   integer(kind=f_integer), dimension(:,:,:,:,:,:,:), intent(out) :: array
   external :: setzero_i1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_i1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_i7

subroutine f_zero_li1(array)
   implicit none
   integer(kind=f_long), dimension(:), intent(out) :: array
   external :: setzero_li1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_li1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_li1

subroutine f_zero_li2(array)
   implicit none
   integer(kind=f_long), dimension(:,:), intent(out) :: array
   external :: setzero_li1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_li1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_li2

subroutine f_zero_li3(array)
   implicit none
   integer(kind=f_long), dimension(:,:,:), intent(out) :: array
   external :: setzero_li1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_li1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_li3

subroutine f_zero_li4(array)
   implicit none
   integer(kind=f_long), dimension(:,:,:,:), intent(out) :: array
   external :: setzero_li1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_li1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_li4

subroutine f_zero_li5(array)
   implicit none
   integer(kind=f_long), dimension(:,:,:,:,:), intent(out) :: array
   external :: setzero_li1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_li1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_li5

subroutine f_zero_li6(array)
   implicit none
   integer(kind=f_long), dimension(:,:,:,:,:,:), intent(out) :: array
   external :: setzero_li1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_li1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_li6

subroutine f_zero_li7(array)
   implicit none
   integer(kind=f_long), dimension(:,:,:,:,:,:,:), intent(out) :: array
   external :: setzero_li1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_li1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_li7

subroutine f_zero_b1(array)
   implicit none
   logical(kind=f_byte), dimension(:), intent(out) :: array
   external :: setzero_b1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_b1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_b1

subroutine f_zero_b2(array)
   implicit none
   logical(kind=f_byte), dimension(:,:), intent(out) :: array
   external :: setzero_b1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_b1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_b2

subroutine f_zero_b3(array)
   implicit none
   logical(kind=f_byte), dimension(:,:,:), intent(out) :: array
   external :: setzero_b1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_b1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_b3

subroutine f_zero_b4(array)
   implicit none
   logical(kind=f_byte), dimension(:,:,:,:), intent(out) :: array
   external :: setzero_b1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_b1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_b4

subroutine f_zero_b5(array)
   implicit none
   logical(kind=f_byte), dimension(:,:,:,:,:), intent(out) :: array
   external :: setzero_b1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_b1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_b5

subroutine f_zero_b6(array)
   implicit none
   logical(kind=f_byte), dimension(:,:,:,:,:,:), intent(out) :: array
   external :: setzero_b1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_b1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_b6

subroutine f_zero_b7(array)
   implicit none
   logical(kind=f_byte), dimension(:,:,:,:,:,:,:), intent(out) :: array
   external :: setzero_b1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_b1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_b7

subroutine f_zero_l1(array)
   implicit none
   logical, dimension(:), intent(out) :: array
   external :: setzero_l1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_l1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_l1

subroutine f_zero_l2(array)
   implicit none
   logical, dimension(:,:), intent(out) :: array
   external :: setzero_l1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_l1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_l2

subroutine f_zero_l3(array)
   implicit none
   logical, dimension(:,:,:), intent(out) :: array
   external :: setzero_l1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_l1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_l3

subroutine f_zero_l4(array)
   implicit none
   logical, dimension(:,:,:,:), intent(out) :: array
   external :: setzero_l1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_l1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_l4

subroutine f_zero_l5(array)
   implicit none
   logical, dimension(:,:,:,:,:), intent(out) :: array
   external :: setzero_l1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_l1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_l5

subroutine f_zero_l6(array)
   implicit none
   logical, dimension(:,:,:,:,:,:), intent(out) :: array
   external :: setzero_l1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_l1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_l6

subroutine f_zero_l7(array)
   implicit none
   logical, dimension(:,:,:,:,:,:,:), intent(out) :: array
   external :: setzero_l1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_l1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_l7

subroutine f_zero_a1(array)
   implicit none
   character(len=*), dimension(:), intent(out) :: array
   external :: setzero_a1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_a1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_a1

subroutine f_zero_a2(array)
   implicit none
   character(len=*), dimension(:,:), intent(out) :: array
   external :: setzero_a1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_a1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_a2

subroutine f_zero_a3(array)
   implicit none
   character(len=*), dimension(:,:,:), intent(out) :: array
   external :: setzero_a1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_a1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_a3

subroutine f_zero_a4(array)
   implicit none
   character(len=*), dimension(:,:,:,:), intent(out) :: array
   external :: setzero_a1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_a1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_a4

subroutine f_zero_a5(array)
   implicit none
   character(len=*), dimension(:,:,:,:,:), intent(out) :: array
   external :: setzero_a1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_a1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_a5

subroutine f_zero_a6(array)
   implicit none
   character(len=*), dimension(:,:,:,:,:,:), intent(out) :: array
   external :: setzero_a1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_a1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_a6

subroutine f_zero_a7(array)
   implicit none
   character(len=*), dimension(:,:,:,:,:,:,:), intent(out) :: array
   external :: setzero_a1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_a1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_a7

subroutine f_zero_c1(array)
   implicit none
   complex(kind=f_simple), dimension(:), intent(out) :: array
   external :: setzero_c1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_c1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_c1

subroutine f_zero_c2(array)
   implicit none
   complex(kind=f_simple), dimension(:,:), intent(out) :: array
   external :: setzero_c1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_c1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_c2

subroutine f_zero_c3(array)
   implicit none
   complex(kind=f_simple), dimension(:,:,:), intent(out) :: array
   external :: setzero_c1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_c1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_c3

subroutine f_zero_c4(array)
   implicit none
   complex(kind=f_simple), dimension(:,:,:,:), intent(out) :: array
   external :: setzero_c1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_c1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_c4

subroutine f_zero_c5(array)
   implicit none
   complex(kind=f_simple), dimension(:,:,:,:,:), intent(out) :: array
   external :: setzero_c1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_c1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_c5

subroutine f_zero_c6(array)
   implicit none
   complex(kind=f_simple), dimension(:,:,:,:,:,:), intent(out) :: array
   external :: setzero_c1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_c1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_c6

subroutine f_zero_c7(array)
   implicit none
   complex(kind=f_simple), dimension(:,:,:,:,:,:,:), intent(out) :: array
   external :: setzero_c1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_c1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_c7

subroutine f_zero_z1(array)
   implicit none
   complex(kind=f_double), dimension(:), intent(out) :: array
   external :: setzero_z1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_z1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_z1

subroutine f_zero_z2(array)
   implicit none
   complex(kind=f_double), dimension(:,:), intent(out) :: array
   external :: setzero_z1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_z1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_z2

subroutine f_zero_z3(array)
   implicit none
   complex(kind=f_double), dimension(:,:,:), intent(out) :: array
   external :: setzero_z1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_z1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_z3

subroutine f_zero_z4(array)
   implicit none
   complex(kind=f_double), dimension(:,:,:,:), intent(out) :: array
   external :: setzero_z1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_z1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_z4

subroutine f_zero_z5(array)
   implicit none
   complex(kind=f_double), dimension(:,:,:,:,:), intent(out) :: array
   external :: setzero_z1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_z1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_z5

subroutine f_zero_z6(array)
   implicit none
   complex(kind=f_double), dimension(:,:,:,:,:,:), intent(out) :: array
   external :: setzero_z1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_z1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_z6

subroutine f_zero_z7(array)
   implicit none
   complex(kind=f_double), dimension(:,:,:,:,:,:,:), intent(out) :: array
   external :: setzero_z1
   call f_timer_interrupt(TCAT_INIT_TO_ZERO)
   call setzero_z1(array, product(int(shape(array),kind=f_long)))
   call f_timer_resume()
end subroutine f_zero_z7

end module f_zero_module

subroutine setzero_r1(array, size)
   use f_precisions
   implicit none
   integer(kind=f_long), intent(in) :: size
   real(kind=f_simple), dimension(size), intent(out) :: array
   !local variables
   integer(kind=f_long) :: i
   !$ logical :: do_omp
   !$ logical, external :: omp_in_parallel

   !$ do_omp = size > int(1024,kind=f_long)
   !$ if (do_omp) do_omp= .not. omp_in_parallel()
   !$omp parallel do if (do_omp) default(none) shared(array,size) private(i)
   do i = 1, size
     array(i) = real(0,kind=f_simple)
   end do
   !$omp end parallel do
end subroutine setzero_r1

subroutine setzero_d1(array, size)
   use f_precisions
   implicit none
   integer(kind=f_long), intent(in) :: size
   real(kind=f_double), dimension(size), intent(out) :: array
   !local variables
   integer(kind=f_long) :: i
   !$ logical :: do_omp
   !$ logical, external :: omp_in_parallel

   !$ do_omp = size > int(1024,kind=f_long)
   !$ if (do_omp) do_omp= .not. omp_in_parallel()
   !$omp parallel do if (do_omp) default(none) shared(array,size) private(i)
   do i = 1, size
     array(i) = real(0,kind=f_double)
   end do
   !$omp end parallel do
end subroutine setzero_d1

! subroutine setzero_q1(array, size)
!    use f_precisions
!    implicit none
!    integer(kind=f_long), intent(in) :: size
!    real(kind=f_quadruple), dimension(size), intent(out) :: array
!    !local variables
!    integer(kind=f_long) :: i
!    !$ logical :: do_omp
!    !$ logical, external :: omp_in_parallel

!    !$ do_omp = size > int(1024,kind=f_long)
!    !$ if (do_omp) do_omp= .not. omp_in_parallel()
!    !$omp parallel do if (do_omp) default(none) shared(array,size) private(i)
!    do i = 1, size
!      array(i) = real(0,kind=f_quadruple)
!    end do
!    !$omp end parallel do
! end subroutine setzero_q1

subroutine setzero_s1(array, size)
   use f_precisions
   implicit none
   integer(kind=f_long), intent(in) :: size
   integer(kind=f_short), dimension(size), intent(out) :: array
   !local variables
   integer(kind=f_long) :: i
   !$ logical :: do_omp
   !$ logical, external :: omp_in_parallel

   !$ do_omp = size > int(1024,kind=f_long)
   !$ if (do_omp) do_omp= .not. omp_in_parallel()
   !$omp parallel do if (do_omp) default(none) shared(array,size) private(i)
   do i = 1, size
     array(i) = int(0,kind=f_short)
   end do
   !$omp end parallel do
end subroutine setzero_s1

subroutine setzero_i1(array, size)
   use f_precisions
   implicit none
   integer(kind=f_long), intent(in) :: size
   integer(kind=f_integer), dimension(size), intent(out) :: array
   !local variables
   integer(kind=f_long) :: i
   !$ logical :: do_omp
   !$ logical, external :: omp_in_parallel

   !$ do_omp = size > int(1024,kind=f_long)
   !$ if (do_omp) do_omp= .not. omp_in_parallel()
   !$omp parallel do if (do_omp) default(none) shared(array,size) private(i)
   do i = 1, size
     array(i) = int(0,kind=f_integer)
   end do
   !$omp end parallel do
end subroutine setzero_i1

subroutine setzero_li1(array, size)
   use f_precisions
   implicit none
   integer(kind=f_long), intent(in) :: size
   integer(kind=f_long), dimension(size), intent(out) :: array
   !local variables
   integer(kind=f_long) :: i
   !$ logical :: do_omp
   !$ logical, external :: omp_in_parallel

   !$ do_omp = size > int(1024,kind=f_long)
   !$ if (do_omp) do_omp= .not. omp_in_parallel()
   !$omp parallel do if (do_omp) default(none) shared(array,size) private(i)
   do i = 1, size
     array(i) = int(0,kind=f_long)
   end do
   !$omp end parallel do
end subroutine setzero_li1

subroutine setzero_b1(array, size)
   use f_precisions
   implicit none
   integer(kind=f_long), intent(in) :: size
   logical(kind=f_byte), dimension(size), intent(out) :: array
   !local variables
   integer(kind=f_long) :: i
   !$ logical :: do_omp
   !$ logical, external :: omp_in_parallel

   !$ do_omp = size > int(1024,kind=f_long)
   !$ if (do_omp) do_omp= .not. omp_in_parallel()
   !$omp parallel do if (do_omp) default(none) shared(array,size) private(i)
   do i = 1, size
     array(i) = logical(.false.,kind=f_byte)
   end do
   !$omp end parallel do
end subroutine setzero_b1

subroutine setzero_l1(array, size)
   use f_precisions
   implicit none
   integer(kind=f_long), intent(in) :: size
   logical, dimension(size), intent(out) :: array
   !local variables
   integer(kind=f_long) :: i
   !$ logical :: do_omp
   !$ logical, external :: omp_in_parallel

   !$ do_omp = size > int(1024,kind=f_long)
   !$ if (do_omp) do_omp= .not. omp_in_parallel()
   !$omp parallel do if (do_omp) default(none) shared(array,size) private(i)
   do i = 1, size
     array(i) = logical(.false.)
   end do
   !$omp end parallel do
end subroutine setzero_l1

subroutine setzero_a1(array, size)
   use f_precisions
   implicit none
   integer(kind=f_long), intent(in) :: size
   character(len=*), dimension(size), intent(out) :: array
   !local variables
   integer(kind=f_long) :: i
   !$ logical :: do_omp
   !$ logical, external :: omp_in_parallel

   !$ do_omp = size > int(1024,kind=f_long)
   !$ if (do_omp) do_omp= .not. omp_in_parallel()
   !$omp parallel do if (do_omp) default(none) shared(array,size) private(i)
   do i = 1, size
     array(i) = ' '
   end do
   !$omp end parallel do
end subroutine setzero_a1

subroutine setzero_c1(array, size)
   use f_precisions
   implicit none
   integer(kind=f_long), intent(in) :: size
   complex(kind=f_simple), dimension(size), intent(out) :: array
   !local variables
   integer(kind=f_long) :: i
   !$ logical :: do_omp
   !$ logical, external :: omp_in_parallel

   !$ do_omp = size > int(1024,kind=f_long)
   !$ if (do_omp) do_omp= .not. omp_in_parallel()
   !$omp parallel do if (do_omp) default(none) shared(array,size) private(i)
   do i = 1, size
     array(i) = cmplx(0,kind=f_simple)
   end do
   !$omp end parallel do
end subroutine setzero_c1

subroutine setzero_z1(array, size)
   use f_precisions
   implicit none
   integer(kind=f_long), intent(in) :: size
   complex(kind=f_double), dimension(size), intent(out) :: array
   !local variables
   integer(kind=f_long) :: i
   !$ logical :: do_omp
   !$ logical, external :: omp_in_parallel

   !$ do_omp = size > int(1024,kind=f_long)
   !$ if (do_omp) do_omp= .not. omp_in_parallel()
   !$omp parallel do if (do_omp) default(none) shared(array,size) private(i)
   do i = 1, size
     array(i) = cmplx(0,kind=f_double)
   end do
   !$omp end parallel do
end subroutine setzero_z1


!should be removed and placed in a suitable module
!> Routine doing daxpy with dx in real(kind=4)
subroutine dasxpdy(n,da,dx,incx,dy,incy)
  implicit none
  integer, intent(in) :: n,incx,incy
  real(kind=8), intent(in) :: da
  real(kind=4), dimension(*), intent(in) :: dx
  real(kind=8), dimension(*), intent(inout) :: dy
  !local variables
  integer :: i,ix,iy
  
  ix=1
  iy=1
  do i=1,n
     dy(iy)=dy(iy)+da*real(dx(ix),kind=8)
     ix=ix+incx
     iy=iy+incy
  end do
end subroutine dasxpdy


!> Copy from real(kind=4) into real(kind=8)
subroutine dscopy(n,dx,incx,dy,incy)
  implicit none
  integer, intent(in) :: n,incx,incy
  real(kind=8), dimension(*), intent(in) :: dx
  real(kind=4), dimension(*), intent(out) :: dy
  !local variables
  integer :: i,ix,iy
  
  ix=1
  iy=1
  do i=1,n
     dy(iy)=real(dx(ix),kind=4)
     ix=ix+incx
     iy=iy+incy
  end do

end subroutine dscopy


!> dcopy for integer arrays
subroutine icopy(n,dx,incx,dy,incy)
  implicit none
  integer, intent(in) :: n,incx,incy
  integer, dimension(*), intent(in) :: dx
  integer, dimension(*), intent(out) :: dy
  !local variables
  integer :: i,ix,iy
  
  ix=1
  iy=1
  do i=1,n
     dy(iy)=dx(ix)
     ix=ix+incx
     iy=iy+incy
  end do

end subroutine icopy

subroutine diff_i(n,a,b,diff,idiff)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  integer, dimension(n), intent(in) :: a
  integer, dimension(n), intent(in) :: b
  integer, intent(out) :: diff
  integer(f_long), intent(out) :: idiff
  !local variables
  integer(f_long) :: i

  diff=0
  do i=1,n
     if (diff < abs(a(i)-b(i))) then
        diff=max(diff,abs(a(i)-b(i)))
        idiff=i
     end if
  end do
end subroutine diff_i


subroutine diff_li(n,a,b,diff,idiff)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  integer(kind=8), dimension(n), intent(in) :: a
  integer(kind=8), dimension(n), intent(in) :: b
  integer(kind=8), intent(out) :: diff
  integer(f_long), intent(out) :: idiff
  !local variables
  integer(f_long) :: i

  diff=int(0,kind=8)
  do i=1,n
     if (diff < abs(a(i)-b(i))) then
        diff=max(diff,abs(a(i)-b(i)))
        idiff=i
     end if
  end do
end subroutine diff_li


subroutine diff_r(n,a,b,diff,idiff)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  real, dimension(n), intent(in) :: a
  real, dimension(n), intent(in) :: b
  real, intent(out) :: diff
  integer(f_long), intent(out) :: idiff
  !local variables
  integer(f_long) :: i

  diff=0.0e0
  do i=1,n
     if (diff < abs(a(i)-b(i))) then
        diff=max(diff,abs(a(i)-b(i)))
        idiff=i
     end if
  end do
end subroutine diff_r


subroutine diff_d(n,a,b,diff,idiff)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  double precision, dimension(n), intent(in) :: a
  double precision, dimension(n), intent(in) :: b
  double precision, intent(out) :: diff
  integer(f_long), intent(out) :: idiff
  !local variables
  integer(f_long) :: i

  diff=0.0d0
  do i=1,n
     if (diff < abs(a(i)-b(i))) then
        diff=max(diff,abs(a(i)-b(i)))
        idiff=i
     end if
  end do
end subroutine diff_d


subroutine diff_l(n,a,b,diff)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  logical, dimension(n), intent(in) :: a
  logical, dimension(n), intent(in) :: b
  logical, intent(out) :: diff
  !local variables
  integer(f_long) :: i

  diff=.false.
  do i=1,n
     diff=a(i) .eqv. b(i)
     if (diff) exit
  end do
end subroutine diff_l


subroutine diff_ci(n,a,b,diff,idiff)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  character, dimension(n), intent(in) :: a
  integer, dimension(n), intent(in) :: b
  integer, intent(out) :: diff
  integer(f_long), intent(out) :: idiff
  !local variables
  integer(f_long) :: i

  diff=0
  do i=1,n
     if (diff < abs(ichar(a(i))-b(i))) then
        diff=max(diff,abs(ichar(a(i))-b(i)))
        idiff=i
     end if
  end do
end subroutine diff_ci


subroutine f_itoa(n,src,dest)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  integer(kind=4), dimension(n), intent(in) :: src
  character, dimension(n), intent(out) :: dest
  !local variables
  integer(f_long) :: i
  
  do i=1,n
     dest(i)=achar(src(i))
  end do

end subroutine f_itoa


subroutine f_litoa(n,src,dest)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  integer(f_long), dimension(n), intent(in) :: src
  character, dimension(n), intent(out) :: dest
  !local variables
  integer(f_long) :: i
  
  do i=1,n
     dest(i)=achar(src(i))
  end do

end subroutine f_litoa

subroutine f_atoi(n,src,dest)
  use f_precisions, only: f_integer,f_long
  implicit none
  integer(f_long), intent(in) :: n
  character, dimension(n), intent(in) :: src
  integer(f_integer), dimension(n), intent(out) :: dest
  !local variables
  integer(f_long) :: i
  
  do i=1,n
     dest(i)=ichar(src(i))
  end do

end subroutine f_atoi

subroutine f_atoli(n,src,dest)
  use f_precisions, only: f_long
  implicit none
  integer(f_long), intent(in) :: n
  character, dimension(n), intent(in) :: src
  integer(f_long), dimension(n), intent(out) :: dest
  !local variables
  integer(f_long) :: i
  
  do i=1,n
     dest(i)=ichar(src(i))
  end do

end subroutine f_atoli

