FUTILE_SPEC = {'f_simple': {'letter': 'r', 'type': 'real'},
               'f_double': {'letter': 'd', 'type': 'real'},
               'f_quadruple': {'letter': 'q', 'type': 'real'},
               'f_short': {'letter': 's', 'type': 'integer'},
               'f_integer': {'letter': 'i', 'type': 'integer'},
               'f_long': {'letter': 'li', 'type': 'integer'},
               'f_address': {'letter': 'add', 'type': 'integer'},
               'f_byte': {'letter': 'b', 'type': 'logical'},
               'f_logical': {'letter': 'l', 'kind': 'default', 'type': 'logical'},
               'f_char': {'letter': 'a', 'type': 'character'},
               'f_complex': {'letter': 'c', 'kind': 'f_simple', 'type': 'complex'},
               'f_double_complex': {'letter': 'z', 'kind': 'f_double', 'type': 'complex'}}

DERIVED = ['f_address']

REALS = ['r', 'd', 'q']

INTEGERS = ['i', 'li', 's', 'add']

COMPLEXES = ['c','z']

LOGICALS = ['l', 'b']


def func_name(basename, intrinsic, rank=None):
    return '_'.join([basename,intrinsic.letter+str(intrinsic.rank if rank is None else rank)])

class Intrinsic():
    def __init__(self, name=None, letter=None, rank='scalar'):
        if name is not None:
            self.name = name
            self.letter = FUTILE_SPEC[name]['letter']
        elif letter is not None:
            self.letter = letter
            found = False
            for nm, sp in FUTILE_SPEC.items():
                if sp['letter'] == letter:
                    found = True
                    break
            if not found:
                raise ValueError('Letter not found', letter)
            self.name = nm
        self.rank = rank if rank != 'scalar' else ''
        self.kind = FUTILE_SPEC[self.name].get('kind', self.name)
        if self.name == 'f_char':
            self.specifier = '(len=*)'
        else:
            self.specifier = '(kind='+self.kind +')' if self.kind != 'default' else ''
        self.type = FUTILE_SPEC[self.name]['type']+self.specifier
        self.conversion = ''
        if self.letter in REALS:
            self.conversion = 'real'
        elif self.letter in INTEGERS:
            self.conversion = 'int'
        elif self.letter in COMPLEXES:
            self.conversion = 'cmplx'
        elif self.letter in LOGICALS:
            self.conversion = 'logical'

    def declaration(self, bounds=None):
        if self.rank in ['', 0]:
            dim = ''
        elif bounds is None:
            dim = ', dimension('+','.join(self.rank*[':'])+')'
        else:
            assert len(bounds) == self.rank, 'Illegal bounds'
            dim = ', dimension('+','.join(bounds)+')'
        return self.type + dim

    def constant(self, value):
        if len(self.conversion) == 0:  # character case
            return "' '"  if value == '0' else value        
        letter = self.letter
        if letter in LOGICALS:
            val = '.false.' if value == '0' else '.true.' 
        else:
            val = value
        return self.conversion+'('+val+self.specifier.replace('(',',').replace(')', '')+')'


AllScalars = [Intrinsic(name=nm) for nm in FUTILE_SPEC if nm not in DERIVED]

