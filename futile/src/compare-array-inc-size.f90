    if (any(shape(val) /= shape(expected))) then
       if (present(label)) then
          call f_err_throw(label // ": different shape, expected " // &
               trim(yaml_toa(shape(expected))) // ", got " // trim(yaml_toa(shape(val))))
       else
          call f_err_throw("different shape, expected " // &
               trim(yaml_toa(shape(expected))) // ", got " // trim(yaml_toa(shape(val))))
       end if
       return
    end if
