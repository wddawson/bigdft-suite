!> @file
!! Include fortran file for allreduce operations

!! @author
!!    Copyright (C) 2017-2017 BigDFT group
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS
  type(f_enumerator), intent(in), optional :: op
  integer, intent(in), optional :: comm
  type(fmpi_irecvbuf), intent(out), optional :: irecvbuf
  !local variables
  integer :: ncount

  if (.not. present(op)) call f_err_throw('MPI_OP should be present in the multiple fmpi_allred',&
       err_id=ERR_MPI_WRAPPERS)

  call f_zero(tmpsend)
  tmpsend(1)=val1
  tmpsend(2)=val2
  ncount=2
  if (present(val3)) then
    tmpsend(3)=val3
    ncount=ncount+1
  end if
  if (present(val4)) then
    tmpsend(4)=val4
    ncount=ncount+1
  end if
  if (present(val5)) then
    tmpsend(5)=val5
    ncount=ncount+1
  end if 
  if (present(val6)) then
    tmpsend(6)=val6
    ncount=ncount+1
  end if
  if (present(val7)) then
    tmpsend(7)=val7
    ncount=ncount+1
  end if
  if (present(val8)) then
    tmpsend(8)=val8
    ncount=ncount+1
  end if
  if (present(val9)) then
    tmpsend(9)=val9
    ncount=ncount+1
  end if 
  if (present(val10)) then
    tmpsend(10)=val10
    ncount=ncount+1
  end if


  if (present(irecvbuf)) then
    call fmpi_allreduce(tmpsend(1:ncount),irecvbuf=irecvbuf,op=op,comm=comm)
  else

    call fmpi_allreduce(sendbuf=tmpsend,recvbuf=tmprecv,op=op,comm=comm)

    val1=tmprecv(1)
    val2=tmprecv(2)
    if (present(val3)) val3=tmprecv(3)
    if (present(val4)) val4=tmprecv(4)
    if (present(val5)) val5=tmprecv(5)
    if (present(val6)) val6=tmprecv(6)
    if (present(val7)) val7=tmprecv(7)
    if (present(val8)) val8=tmprecv(8)
    if (present(val9)) val9=tmprecv(9)
    if (present(val10)) val10=tmprecv(10)
  end if