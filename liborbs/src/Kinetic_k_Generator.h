#ifndef KINETIC_K_GENERATOR_H
#define KINETIC_K_GENERATOR_H

#include "liborbs_ocl.h"

#ifdef __cplusplus
extern "C" char* generate_kinetic_k_program(struct liborbs_device_infos * infos);
#else
char* generate_kinetic_k_program(struct liborbs_device_infos * infos);
#endif

#endif
