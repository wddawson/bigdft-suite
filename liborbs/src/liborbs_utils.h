#ifndef LIBORBS_UTILS_H
#define LIBORBS_UTILS_H

#define CL_TARGET_OPENCL_VERSION 220
#include <CL/opencl.h>
#include <stdio.h>
#include <assert.h>

#define ERR_STRING(n,s) switch(n)\
    { case  0:  s="CL_SUCCESS"; break;\
      case -1:  s="CL_DEVICE_NOT_FOUND"; break;\
      case -2:  s="CL_DEVICE_NOT_AVAILABLE"; break;\
      case -3:  s="CL_COMPILER_NOT_AVAILABLE"; break;\
      case -4:  s="CL_MEM_OBJECT_ALLOCATION_FAILURE"; break;\
      case -5:  s="CL_OUT_OF_RESOURCES"; break;\
      case -6:  s="CL_OUT_OF_HOST_MEMORY"; break;\
      case -7:  s="CL_PROFILING_INFO_NOT_AVAILABLE"; break;\
      case -8:  s="CL_MEM_COPY_OVERLAP"; break;\
      case -9:  s="CL_IMAGE_FORMAT_MISMATCH"; break;\
      case -10: s="CL_IMAGE_FORMAT_NOT_SUPPORTED"; break;\
      case -11: s="CL_BUILD_PROGRAM_FAILURE"; break;\
      case -12: s="CL_MAP_FAILURE"; break;\
      case -13: s="CL_MISALIGNED_SUB_BUFFER_OFFSET"; break;\
      case -14: s="CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST"; break;\
      case -15: s="CL_COMPILE_PROGRAM_FAILURE"; break;\
      case -16: s="CL_LINKER_NOT_AVAILABLE"; break;\
      case -17: s="CL_LINK_PROGRAM_FAILURE"; break;\
      case -18: s="CL_DEVICE_PARTITION_FAILED"; break;\
      case -19: s="CL_KERNEL_ARG_INFO_NOT_AVAILABLE"; break;\
      case -30: s="CL_INVALID_VALUE"; break;\
      case -31: s="CL_INVALID_DEVICE_TYPE"; break;\
      case -32: s="CL_INVALID_PLATFORM"; break;\
      case -33: s="CL_INVALID_DEVICE"; break;\
      case -34: s="CL_INVALID_CONTEXT"; break;\
      case -35: s="CL_INVALID_QUEUE_PROPERTIES"; break;\
      case -36: s="CL_INVALID_COMMAND_QUEUE"; break;\
      case -37: s="CL_INVALID_HOST_PTR"; break;\
      case -38: s="CL_INVALID_MEM_OBJECT"; break;\
      case -39: s="CL_INVALID_IMAGE_FORMAT_DESCRIPTOR"; break;\
      case -40: s="CL_INVALID_IMAGE_SIZE"; break;\
      case -41: s="CL_INVALID_SAMPLER"; break;\
      case -42: s="CL_INVALID_BINARY"; break;\
      case -43: s="CL_INVALID_BUILD_OPTIONS"; break;\
      case -44: s="CL_INVALID_PROGRAM"; break;\
      case -45: s="CL_INVALID_PROGRAM_EXECUTABLE"; break;\
      case -46: s="CL_INVALID_KERNEL_NAME"; break;\
      case -47: s="CL_INVALID_KERNEL_DEFINITION"; break;\
      case -48: s="CL_INVALID_KERNEL"; break;\
      case -49: s="CL_INVALID_ARG_INDEX"; break;\
      case -50: s="CL_INVALID_ARG_VALUE"; break;\
      case -51: s="CL_INVALID_ARG_SIZE"; break;\
      case -52: s="CL_INVALID_KERNEL_ARGS"; break;\
      case -53: s="CL_INVALID_WORK_DIMENSION"; break;\
      case -54: s="CL_INVALID_WORK_GROUP_SIZE"; break;\
      case -55: s="CL_INVALID_WORK_ITEM_SIZE"; break;\
      case -56: s="CL_INVALID_GLOBAL_OFFSET"; break;\
      case -57: s="CL_INVALID_EVENT_WAIT_LIST"; break;\
      case -58: s="CL_INVALID_EVENT"; break;\
      case -59: s="CL_INVALID_OPERATION"; break;\
      case -60: s="CL_INVALID_GL_OBJECT"; break;\
      case -61: s="CL_INVALID_BUFFER_SIZE"; break;\
      case -62: s="CL_INVALID_MIP_LEVEL"; break;\
      case -63: s="CL_INVALID_GLOBAL_WORK_SIZE"; break;\
      case -64: s="CL_INVALID_PROPERTY"; break;\
      case -65: s="CL_INVALID_IMAGE_DESCRIPTOR"; break;\
      case -66: s="CL_INVALID_COMPILER_OPTIONS"; break;\
      case -67: s="CL_INVALID_LINKER_OPTIONS"; break;\
      case -68: s="CL_INVALID_DEVICE_PARTITION_COUNT"; break;\
      case -1001: s="CL_PLATFORM_NOT_FOUND_KHR"; break;\
      default: s="UNKNOWN_ERROR";\
      }

#define oclErrorCheck(errorCode,message) if(errorCode!=CL_SUCCESS) { char *s; ERR_STRING(errorCode,s) ; fprintf(stderr,"Error(%i)(%s)  (%s: %s): %s\n", errorCode, s,__FILE__,__func__,message); fflush(NULL); exit(1);}

/** Returns the first device available in a given context. */
cl_device_id oclGetFirstDev(cl_context cxGPUContext);

/** Returns the next integer that is equal or greater than global_size and a multiple of group_size. */
size_t shrRoundUp(size_t group_size, size_t global_size);

void v_initialize_generic(cl_kernel kernel, cl_command_queue command_queue, cl_uint *ndat, cl_mem *out, double *c);

void c_initialize_generic(cl_kernel kernel, cl_command_queue command_queue, cl_uint *ndat, cl_mem *in, cl_mem *inout, double *c);

#endif
