!> @file
!!  Routines to precondition
!! @author
!!    Copyright (C) 2010-2011 BigDFT group 
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS 

!>   Applies the operator (KE+cprecr*I)*x=y
!!   array x is input, array y is output
subroutine apply_hp_slab_sd(lr,cprecr,x,y,psifscf,ww,modul1,modul3,a,b,c,e)
  use liborbs_precisions
  use locregs
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  real(gp), intent(in) :: cprecr
  real(wp), dimension(array_dim(lr)), intent(in) :: x
  real(wp), dimension(array_dim(lr)), intent(out) :: y
  integer, parameter :: lowfil=-14,lupfil=14

  !n(c) real(gp) hgrid(3)   
  real(wp),dimension(lr%mesh_fine%ndim)::ww,psifscf

  integer,intent(in)::modul1(lowfil:lr%mesh_coarse%ndims(1)+lupfil-1)
  integer,intent(in)::modul3(lowfil:lr%mesh_coarse%ndims(3)+lupfil-1)
  real(gp),intent(in)::a(lowfil:lupfil,3)
  real(gp),intent(in)::b(lowfil:lupfil,3)
  real(gp),intent(in)::c(lowfil:lupfil,3)
  real(gp),intent(in)::e(lowfil:lupfil,3)

  real(wp), dimension(0:7), parameter :: scal = (/ 1._wp, 1._wp, 1._wp, 1._wp, 1._wp, 1._wp, 1._wp, 1._wp /)

  call apply_hp_slab_sd_scal(lr,cprecr,x,y,psifscf,ww,modul1,modul3,a,b,c,e,scal)
END SUBROUTINE apply_hp_slab_sd


!>   Applies the operator (KE+cprecr*I)*x=y
!!   array x is input, array y is output
subroutine apply_hp_slab_sd_scal(lr,cprecr,x,y,psifscf,ww,modul1,modul3,a,b,c,e,scal)
  use liborbs_precisions
  use locregs
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  real(gp),intent(in) :: scal(0:7)
  real(gp), intent(in) :: cprecr
  real(wp), dimension(array_dim(lr)), intent(in) :: x
  real(wp), dimension(array_dim(lr)), intent(out) :: y
  integer, parameter :: lowfil=-14,lupfil=14

  !n(c) real(gp) hgrid(3)   
  real(wp),dimension(lr%mesh_fine%ndim)::ww,psifscf

  integer,intent(in)::modul1(lowfil:lr%mesh_coarse%ndims(1)+lupfil-1)
  integer,intent(in)::modul3(lowfil:lr%mesh_coarse%ndims(3)+lupfil-1)
  real(gp),intent(in)::a(lowfil:lupfil,3)
  real(gp),intent(in)::b(lowfil:lupfil,3)
  real(gp),intent(in)::c(lowfil:lupfil,3)
  real(gp),intent(in)::e(lowfil:lupfil,3)
  ! x: input
  ! psifscf: output
  call decompress_sd_scal(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), lr%mesh_coarse%ndims(3), &
       x, psifscf, scal)

  ! psifscf: input, ww: output
  !     call convolut_kinetic_slab_c(2*n1+1,2*n2+15,2*n3+1,hgridh,psifscf,ww,cprecr)
  call convolut_kinetic_slab_sdc(lr%mesh_coarse%ndims(1)-1, lr%mesh_coarse%ndims(2)-1, &
       lr%mesh_coarse%ndims(3)-1, psifscf,ww,cprecr,modul1,modul3,a,b,c,e)
  ! ww:intput
  ! y:output
  call compress_sd_scal(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), lr%mesh_coarse%ndims(3), &
       ww, y, scal)

END SUBROUTINE apply_hp_slab_sd_scal


!> Solves (KE+cprecr*I)*xx=yy by conjugate gradient method
!! hpsi is the right hand side on input and the solution on output
subroutine prec_fft_slab_fast(lr,cprecr,hpsi,kern_k1,kern_k3,z,x_c)
  use liborbs_precisions
  use compression
  use locregs
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  real(gp), intent(in) :: cprecr
  real(wp), dimension(array_dim(lr)), intent(inout) :: hpsi

  !work arrays
  real(gp):: kern_k1(lr%mesh_coarse%ndims(1)),kern_k3(lr%mesh_coarse%ndims(3))
  real(wp),dimension(lr%mesh_coarse%ndims(1),lr%mesh_coarse%ndims(2),lr%mesh_coarse%ndims(3)):: x_c! in and out of Fourier preconditioning
  real(wp) :: z(2,lr%mesh_coarse%ndims(1)/2+1,lr%mesh_coarse%ndims(2),lr%mesh_coarse%ndims(3))! work array for FFT

  ! diagonally precondition the wavelet part  
  if (lr%wfd%nvctr_f > 0) then
     call wscal_f(lr%wfd%nvctr_f,hpsi(lr%wfd%nvctr_c+1),lr%mesh_coarse%hgrids(1), &
          lr%mesh_coarse%hgrids(2),lr%mesh_coarse%hgrids(3),cprecr)
  end if

  call make_kernel(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%hgrids(1),kern_k1)
  call make_kernel(lr%mesh_coarse%ndims(3)-1,lr%mesh_coarse%hgrids(3),kern_k3)

  call wfd_decompress(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), &
       lr%mesh_coarse%ndims(3), x_c, hpsi)

  !   solve the helmholtz equation for the scfunction part  
  call hit_with_kernel_slab(x_c,z,kern_k1,kern_k3,lr%mesh_coarse%ndims(1)-1, &
       lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,cprecr,lr%mesh_coarse%hgrids(2))   

  call wfd_compress(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), &
       lr%mesh_coarse%ndims(3), x_c, hpsi)

END SUBROUTINE prec_fft_slab_fast


!> Solves (KE+cprecr*I)*xx=yy by conjugate gradient method
!! hpsi is the right hand side on input and the solution on output
subroutine prec_fft_wire_fast(lr,cprecr,hpsi,kern_k3,x_c)
  use liborbs_precisions
  use compression
  use locregs
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  real(gp), intent(in) :: cprecr
  real(wp), dimension(array_dim(lr)), intent(inout) :: hpsi

  !work arrays
  real(gp):: kern_k3(lr%mesh_coarse%ndims(3))
  real(wp),dimension(lr%mesh_coarse%ndims(1),lr%mesh_coarse%ndims(2),lr%mesh_coarse%ndims(3)):: x_c! in and out of Fourier preconditioning

  ! diagonally precondition the wavelet part  
  if (lr%wfd%nvctr_f > 0) then
     call wscal_f(lr%wfd%nvctr_f,hpsi(lr%wfd%nvctr_c+1),lr%mesh_coarse%hgrids(1), &
          lr%mesh_coarse%hgrids(2),lr%mesh_coarse%hgrids(3),cprecr)
  end if

  call make_kernel(lr%mesh_coarse%ndims(3)-1,lr%mesh_coarse%hgrids(3),kern_k3)

  call wfd_decompress(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), &
       lr%mesh_coarse%ndims(3), x_c, hpsi)

  !   solve the helmholtz equation for the scfunction part  
!  call hit_with_kernel_wire(x_c,z,kern_k3,n1,n2,n3,cprecr,hx,hy)   

  call wfd_compress(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), &
       lr%mesh_coarse%ndims(3), x_c, hpsi)

END SUBROUTINE prec_fft_wire_fast


!> Solves (KE+cprecr*I)*xx=yy by conjugate gradient method
!! hpsi is the right hand side on input and the solution on output
subroutine prec_fft_slab(lr,cprecr,hpsi)
  use liborbs_precisions
  use dynamic_memory
  use locregs
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  real(gp), intent(in) :: cprecr
  real(wp), dimension(array_dim(lr)), intent(inout) :: hpsi
  !local variables
  real(gp), dimension(:), allocatable :: kern_k1,kern_k3
  real(wp), dimension(:,:,:), allocatable :: x_c! in and out of Fourier preconditioning
  real(wp), allocatable::z(:,:,:,:) ! work array for FFT

  call allocate_all

  call prec_fft_slab_fast(lr, cprecr, hpsi, kern_k1, kern_k3, z, x_c)

  call deallocate_all

contains

  subroutine allocate_all
    use dynamic_memory
    kern_k1 = f_malloc(lr%mesh_coarse%ndims(1),id='kern_k1')
    kern_k3 = f_malloc(lr%mesh_coarse%ndims(3),id='kern_k3')
    z = f_malloc((/ 2, lr%mesh_coarse%ndims(1)/2+1, lr%mesh_coarse%ndims(2), lr%mesh_coarse%ndims(3) /),id='z')
    x_c = f_malloc(lr%mesh_coarse%ndims,id='x_c')
  END SUBROUTINE allocate_all

  subroutine deallocate_all
    use dynamic_memory
    call f_free(z)
    call f_free(kern_k1)
    call f_free(kern_k3)
    call f_free(x_c)
  END SUBROUTINE deallocate_all

END SUBROUTINE prec_fft_slab


!> Solves (KE+cprecr*I)*xx=yy by conjugate gradient method
!! hpsi is the right hand side on input and the solution on output
subroutine prec_fft_wire(lr,cprecr,hpsi)
  use liborbs_precisions
  use dynamic_memory
  use locregs
  implicit none 
  type(locreg_descriptors), intent(in) :: lr
  real(gp), intent(in) :: cprecr
  real(wp), dimension(array_dim(lr)), intent(inout) :: hpsi
  !local variables
  real(gp), dimension(:), allocatable :: kern_k3
  real(wp), dimension(:,:,:), allocatable :: x_c! in and out of Fourier preconditioning

  call allocate_all

  call prec_fft_wire_fast(lr, cprecr, hpsi, kern_k3, x_c)

  call deallocate_all

contains

  subroutine allocate_all
    kern_k3 = f_malloc(lr%mesh_coarse%ndims(3),id='kern_k3')
    x_c = f_malloc(lr%mesh_coarse%ndims,id='x_c')
  END SUBROUTINE allocate_all

  subroutine deallocate_all
    call f_free(kern_k3)
    call f_free(x_c)
  END SUBROUTINE deallocate_all

END SUBROUTINE prec_fft_wire


!> Solve the discretized equation
!! (-d^2/dy^2+ct(k1,k3)) zx(output) = zx(input)
!! for all k1,k3 via Lapack
subroutine segment_invert(n1,n2,n3,kern_k1,kern_k3,c,zx,hgrid)
  use liborbs_precisions
  use dynamic_memory
  implicit none
  integer,intent(in) :: n1,n2,n3
  real(wp),intent(in) :: kern_k1(0:n1)
  real(wp),intent(in) :: kern_k3(0:n3)
  real(gp),intent(in) :: c,hgrid
  real(wp),intent(inout) :: zx(2,0:(n1+1)/2,0:n2,0:n3)

  real(gp) :: ct
  real(gp),allocatable,dimension(:,:), save :: ab
  !real(wp) :: b(0:n2,2)
  real(wp),allocatable,dimension(:,:), save :: b
  !     .. Scalar Arguments ..
  INTEGER :: NRHS=2
  INTEGER :: INFO, Kd, LDAB, LDB, n
  integer :: i1,i2,i3,i,j

  integer,parameter :: lowfil=-14,lupfil=14
  real(gp) :: scale
  real(gp) :: fil(lowfil:lupfil)
  !!$omp threadprivate(b,ab)

  call f_routine(id='segment_invert')
  
  scale=-.5_gp/hgrid**2

  ! second derivative filters for Daubechies 16
  fil(0)=   -3.5536922899131901941296809374_gp*scale
  fil(1)=    2.2191465938911163898794546405_gp*scale
  fil(2)=   -0.6156141465570069496314853949_gp*scale
  fil(3)=    0.2371780582153805636239247476_gp*scale
  fil(4)=   -0.0822663999742123340987663521_gp*scale
  fil(5)=    0.02207029188482255523789911295638968409_gp*scale
  fil(6)=   -0.409765689342633823899327051188315485e-2_gp*scale
  fil(7)=    0.45167920287502235349480037639758496e-3_gp*scale
  fil(8)=   -0.2398228524507599670405555359023135e-4_gp*scale
  fil(9)=    2.0904234952920365957922889447361e-6_gp*scale
  fil(10)=  -3.7230763047369275848791496973044e-7_gp*scale
  fil(11)=  -1.05857055496741470373494132287e-8_gp*scale
  fil(12)=  -5.813879830282540547959250667e-11_gp*scale
  fil(13)=   2.70800493626319438269856689037647576e-13_gp*scale
  fil(14)=  -6.924474940639200152025730585882e-18_gp*scale

  do i=1,14
     fil(-i)=fil(i)
  enddo

  fil=fil*(n1+1)*(n3+1) ! include the factor from the Fourier transform

  ! initialize the variables for matrix inversion
  n=n2+1
  kd=lupfil
  ldab=kd+1!
  ldb=n2+1

  !commenting OMP for the moment due to crash on BG/Q
  ! hit the fourier transform of x with the kernel
  !We avoid to use f_malloc in an omp parallel section
  !!$omp parallel default(none) & 
  !!$omp private (i3,i1,i2,j,i,ct,info) &
  !!$omp shared (n1,n2,n3,zx,fil,kd,ldb,ldab,nrhs,n,c,kern_k1,kern_k3)

  !!$omp critical
  ab = f_malloc((/ ldab, n /),id='ab')
  b = f_malloc((/ 0.to.n2, 1.to.2 /),id='b')
  !!$omp end critical

  !!$omp do schedule(static,1)
  do i3=0,n3
     !   do i1=0,n1
     do i1=0,(n1+1)/2
        !      ct=kern_k1(i1)+kern_k3(i3)+c
        ct=(kern_k1(i1)+kern_k3(i3)+c)*(n1+1)*(n3+1)
        ! ab has to be reinitialized each time
        ! since it is overwritten in the course of  dgbsv
        do j=1,n
           do i=max(1,j-lupfil),j
              ab(kd+1+i-j,j)=fil(i-j)
           enddo
           ab(kd+1,j)=fil(0)+ct
        enddo
        do i2=0,n2
           b(i2,1)=zx(1,i1,i2,i3)
           b(i2,2)=zx(2,i1,i2,i3)
        enddo
        !      call DGBSV( N, KL, KU, NRHS, ab , LDAB, IPIV, b, LDB, INFO )
        call DPBSV( 'U', N, KD, NRHS, AB, LDAB, B, LDB, INFO )
        if (info.ne.0) stop 'error in matrix inversion'
        do i2=0,n2
           zx(1,i1,i2,i3)=b(i2,1)
           zx(2,i1,i2,i3)=b(i2,2)
        enddo
     enddo
  enddo
  !!$omp end do

  !!$omp critical
  call f_free(ab)
  call f_free(b)
  !!$omp end critical

  !!$omp end parallel

call f_release_routine()

END SUBROUTINE segment_invert


!!! subroutine segment_invert(n1,n2,n3,kern_k1,kern_k3,c,zx,hgrid)
!!!   use module_base
!!!   use yaml_output, only: yaml_map
!!!   implicit none
!!!   integer,intent(in) :: n1,n2,n3
!!!   real(wp),intent(in) :: kern_k1(0:n1)
!!!   real(wp),intent(in) :: kern_k3(0:n3)
!!!   real(gp),intent(in) :: c,hgrid
!!!   real(wp),intent(inout) :: zx(2,0:(n1+1)/2,0:n2,0:n3)
!!! 
!!!   real(gp) :: ct
!!!   real(gp),allocatable,dimension(:,:,:) :: ab
!!!   !real(wp) :: b(0:n2,2)
!!!   real(wp),allocatable,dimension(:,:,:) :: b
!!!   !     .. Scalar Arguments ..
!!!   INTEGER :: NRHS=2
!!!   INTEGER :: INFO, Kd, LDAB, LDB, n, itime,jtime
!!!   integer :: i1,i2,i3,i,j
!!!   !$ integer :: nthread,ithread, omp_get_max_threads, omp_get_thread_num
!!! 
!!!   integer,parameter :: lowfil=-14,lupfil=14
!!!   real(gp) :: scale
!!!   real(gp) :: fil(lowfil:lupfil)
!!! 
!!!   call f_routine(id='segment_invert')
!!!   
!!!   scale=-.5_gp/hgrid**2
!!! 
!!!   ! second derivative filters for Daubechies 16
!!!   fil(0)=   -3.5536922899131901941296809374_gp*scale
!!!   fil(1)=    2.2191465938911163898794546405_gp*scale
!!!   fil(2)=   -0.6156141465570069496314853949_gp*scale
!!!   fil(3)=    0.2371780582153805636239247476_gp*scale
!!!   fil(4)=   -0.0822663999742123340987663521_gp*scale
!!!   fil(5)=    0.02207029188482255523789911295638968409_gp*scale
!!!   fil(6)=   -0.409765689342633823899327051188315485e-2_gp*scale
!!!   fil(7)=    0.45167920287502235349480037639758496e-3_gp*scale
!!!   fil(8)=   -0.2398228524507599670405555359023135e-4_gp*scale
!!!   fil(9)=    2.0904234952920365957922889447361e-6_gp*scale
!!!   fil(10)=  -3.7230763047369275848791496973044e-7_gp*scale
!!!   fil(11)=  -1.05857055496741470373494132287e-8_gp*scale
!!!   fil(12)=  -5.813879830282540547959250667e-11_gp*scale
!!!   fil(13)=   2.70800493626319438269856689037647576e-13_gp*scale
!!!   fil(14)=  -6.924474940639200152025730585882e-18_gp*scale
!!! 
!!!   do i=1,14
!!!      fil(-i)=fil(i)
!!!   enddo
!!! 
!!!   fil=fil*(n1+1)*(n3+1) ! include the factor from the Fourier transform
!!! 
!!!   ! initialize the variables for matrix inversion
!!!   n=n2+1
!!!   kd=lupfil
!!!   ldab=kd+1!
!!!   ldb=n2+1
!!! 
!!!   ! hit the fourier transform of x with the kernel
!!! itime=f_time()
!!!   !We avoid to use f_malloc in an omp parallel section
!!!   nthread = 1
!!!   !$ nthread = omp_get_max_threads()
!!!   ab = f_malloc((/ 1.to.ldab, 1.to.n, 0.to.nthread-1 /),id='ab')
!!!   b = f_malloc((/ 0.to.n2, 1.to.2, 0.to.nthread-1 /),id='b')
!!!   !$omp parallel default(none) & 
!!!   !$omp private (i3,i1,i2,j,i,ct,info,ithread) &
!!!   !$omp shared (ab,b,n1,n2,n3,zx,fil,kd,ldb,ldab,nrhs,n,c,kern_k1,kern_k3)
!!!   ithread = 0
!!!   !$ ithread = omp_get_thread_num()
!!!   !$omp do schedule(static,1)
!!!   do i3=0,n3
!!!      !   do i1=0,n1
!!!      do i1=0,(n1+1)/2
!!!         !      ct=kern_k1(i1)+kern_k3(i3)+c
!!!         ct=(kern_k1(i1)+kern_k3(i3)+c)*(n1+1)*(n3+1)
!!!         ! ab has to be reinitialized each time
!!!         ! since it is overwritten in the course of  dgbsv
!!!         do j=1,n
!!!            do i=max(1,j-lupfil),j
!!!               ab(kd+1+i-j,j,ithread)=fil(i-j)
!!!            enddo
!!!            ab(kd+1,j,ithread)=fil(0)+ct
!!!         enddo
!!!         do i2=0,n2
!!!            b(i2,1,ithread)=zx(1,i1,i2,i3)
!!!            b(i2,2,ithread)=zx(2,i1,i2,i3)
!!!         enddo
!!!         !      call DGBSV( N, KL, KU, NRHS, ab , LDAB, IPIV, b, LDB, INFO )
!!!         call DPBSV( 'U', N, KD, NRHS, AB(1,1,ithread), LDAB, B(0,1,ithread), LDB, INFO )
!!!         if (info.ne.0) stop 'error in matrix inversion'
!!!         do i2=0,n2
!!!            zx(1,i1,i2,i3)=b(i2,1,ithread)
!!!            zx(2,i1,i2,i3)=b(i2,2,ithread)
!!!         enddo
!!!      enddo
!!!   enddo
!!!   !$omp end do
!!!   !$omp end parallel
!!!   call f_free(ab)
!!!   call f_free(b)
!!! 
!!! jtime=f_time()
!!! 
!!! call yaml_map('Time elapsed (ns)',jtime-itime)
!!! 
!!! call f_release_routine()
!!! 
!!! END SUBROUTINE segment_invert
