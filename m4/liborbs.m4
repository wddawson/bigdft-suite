# -*- Autoconf -*-
#
# Copyright (c) 2018 BigDFT Group (Luigi Genovese, Damien Caliste)
# All rights reserved.
#
# This file is part of the BigDFT software package. For license information,
# please see the COPYING file in the top-level directory of the BigDFT source
# distribution.
AC_DEFUN([AX_LIBORBS],
[dnl Test for PSolver
AC_REQUIRE([AX_FLIB])
AC_REQUIRE([AX_LINALG])
AC_REQUIRE([AX_ATLAB])
AX_PACKAGE([LIBORBS],[0.1],[-lorbs],[$LIB_ATLAB_LIBS $LIB_FUTILE_LIBS $LINALG_LIBS],[$LIB_FUTILE_CFLAGS],
[program main
  use compression
  type(wavefunctions_descriptors) :: wfd
  call daub_to_isf()
  end program],
  [call daub_to_isf()])
if test $ax_have_LIBORBS != "yes" ; then
  AC_MSG_ERROR([Liborbs library not found, cannot proceed.])
fi
])
