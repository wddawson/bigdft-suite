<?xml version="1.0"?><!--*- mode: nxml; indent-tabs-mode: nil -*-->
<!DOCTYPE moduleset SYSTEM "moduleset.dtd">
<?xml-stylesheet type="text/xsl" href="moduleset.xsl"?>
<!-- vim:set ts=2 expandtab: -->
<moduleset>

  <repository type="tarball" name="local" href="./"/>
  <repository type="tarball" name="pyfutile" href="./futile/python/"/>

  <include href="upstream.modules"/>

  <autotools id="libABINIT" check-target="false" autogen-sh="autoreconf">
    <branch repo="local" module="libABINIT-6.8+12.tar.gz" version="6.8+12" checkoutdir="libABINIT">
    </branch>
    <dependencies>
      <dep package="futile"/>
      <if condition-unset="no_upstream">
         <dep package="libxc"/>
      </if>
    </dependencies>
  </autotools>

  <autotools id="futile" autogen-sh="autoreconf">
    <branch repo="local" module="futile-1.9.tar.gz" version="1.9" checkoutdir="futile"/>
    <dependencies>
      <if condition-unset="no_upstream">
         <dep package="libyaml"/>
         <dep package="PyYAML"/>
         <dep package="futile-plugins"/>
       </if>
    </dependencies>
  </autotools>

  <distutils id="pyfutile">
    <branch repo="local" module="pyFutile-1.9.3.tar.gz" version="1.9.3"
       checkoutdir="futile" source-subdir='python'/>
    <dependencies>
      <if condition-unset="no_upstream">
        <dep package="PyYAML"/>
        <if condition-set="devdoc">
          <dep package="sphinx-fortran"/>
        </if>
      </if>
    </dependencies>
  </distutils>

  <autotools id="psolver" autogen-sh="autoreconf">
    <branch repo="local" module="psolver-1.9.tar.gz" version="1.9" checkoutdir="psolver"/>
    <dependencies>
      <dep package="futile"/>
      <dep package="atlab"/>
      <if condition-set="sycl">
         <dep package="dbfft"/>
      </if>
    </dependencies>
  </autotools>

  <autotools id="atlab" autogen-sh="autoreconf">
    <branch repo="local" module="atlab-1.0.tar.gz" version="1.0.0" checkoutdir="atlab"/>
    <dependencies>
      <dep package="futile"/>
      <if condition-set="babel">
        <if condition-unset="no_upstream">
          <dep package="openbabel"/>
        </if>
      </if>
    </dependencies>
  </autotools>

  <autotools id="chess" autogen-sh="autoreconf">
    <branch repo="local" module="CheSS-0.2.4.tar.gz" version="0.2.4" checkoutdir="chess"/>
    <dependencies>
      <dep package="futile"/>
      <dep package="atlab"/>
      <if condition-unset="no_upstream">
          <dep package="ntpoly"/>
      </if>
    </dependencies>
    <autogenargs value="--enable-ntpoly"/>
  </autotools>

  <autotools id="liborbs" autogen-sh="autoreconf">
    <branch repo="local" module="liborbs-0.1.tar.gz" version="0.1" checkoutdir="liborbs"/>
    <dependencies>
      <dep package="futile"/>
      <dep package="atlab"/>
    </dependencies>
  </autotools>

  <autotools id="bigdft" autogen-sh="autoreconf">
    <branch repo="local" module="bigdft-1.9.4.*.tar.gz" version="1.9.4" checkoutdir="bigdft"/>
    <dependencies>
      <dep package="futile"/>
      <dep package="atlab"/>
      <dep package="chess"/>
      <if condition-unset="no_upstream">
         <dep package="core-upstream-suite"/>
      </if>
      <dep package="psolver"/>
      <dep package="libABINIT"/>
      <dep package="liborbs"/>
      <dep package="PyBigDFT"/>
    </dependencies>
    <autogenargs value="--enable-ntpoly"/>
  </autotools>

  <autotools id="spred" autogen-sh="autoreconf">
    <branch repo="local" module="spred-1.8.tar.gz" version="1.8.0" checkoutdir="spred"/>
    <dependencies>
      <dep package="futile"/>
      <dep package="psolver"/>
      <dep package="bigdft"/>
    </dependencies>
  </autotools>

  <distutils id="PyBigDFT">
    <branch repo="local" module="PyBigDFT-1.0.6.tar.gz" version="1.0.6" checkoutdir="PyBigDFT"/>
    <dependencies>
      <if condition-unset="no_upstream">
        <dep package="client-upstream-suite"/>
      </if>
    </dependencies>
  </distutils>

  <metamodule id="bigdft-suite">
    <dependencies>
      <dep package="spred"/>
      <dep package="bigdft"/>
    </dependencies>
  </metamodule>

  <metamodule id="bigdft-client">
    <dependencies>
      <dep package="PyBigDFT"/>
      <dep package="pyfutile"/>
    </dependencies>
  </metamodule>

  <metamodule id="spred-suite">
    <dependencies>
      <dep package="spred"/>
      <dep package="bigdft-suite"/>
    </dependencies>
  </metamodule>

  <metamodule id="chess-suite">
    <dependencies>
      <dep package="chess"/>
    </dependencies>
  </metamodule>

</moduleset>
