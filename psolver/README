/*! @file
@brief README of the source directory of the PSolver library

@dir ./
@brief Directory containing the PSolver library

@mainpage PSolver developer documentation
This Poisson Solver is part of the BigDFT project but can also be used separately.
All the source files of this directory
are delivered under the GNU General Public License; please see the file COPYING 
for copying conditions.

See the file INSTALL for generic compilation and installation instructions.

This Poisson Solver code is built following the method described in the papers:

- G. Fisicaro, L. Genovese, O. Aindreussi, N. Marzari, S. Goedecker
    + Journal of Chemical Physics, 144 (1), 014103 (2016)
    + A generalized Poisson and Poisson-Boltzmann solver for electrostatic environments
    + http://dx.doi.org/10.1063/1.4939125

- A. Cerioni, L. Genovese, A. Mirone, V. A. Sole
    + Journal of Chemical Physics, 137, 134108 (2012)
    + Efficient and accurate solver of the three-dimensional screened and unscreened Poisson's equation with generic boundary conditions
    + http://dx.doi.org/10.1063/1.4755349

- L.Genovese, T. Deutsch, S. Goedecker,
    + Journal of Chemical Physics, 127 (5), 054704 (2007).
    + Efficient and accurate three-dimensional Poisson solver for surface problems.
    + http://dx.doi.org/10.1063/1.2754685

- L. Genovese, T. Deutsch, A. Neelov, S. Goedecker, G. Beylkin,
    + Journal of Chemical Physics, 125 (7), 074105 (2006).
    + Efficient solution of Poisson's equation with free boundary conditions
    + http://dx.doi.org/10.1063/1.2335442

Citing of this references is greatly appreciated if the routines are used 
for scientific work.

@dir ./src
@brief Directory containing the source of the PSolver library

@dir ./tests
@brief Contains some tests for the PSolver library

@parblock
     Copyright (C) 2008-2017 BigDFT Group

     This file is part of BigDFT.

     This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2, or (at your option)
     any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program; see the file COPYING. If not, write to
     the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
@endparblock
*/

*/
