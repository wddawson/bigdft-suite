!> @file
!!    Modulefile for handling basic definitions for the PSolver
!!
!! @author
!!    G. Fisicaro, L. Genovese (September 2015)
!!    Copyright (C) 2002-2015 BigDFT group 
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS 
module PSbase
  use f_precisions
  use time_profiling, only: TIMING_UNINITIALIZED
  implicit none
  private
  ! General precision, density and the potential types, to be moved in a low-levle module
  integer, parameter, public :: gp=f_double  !< general-type precision
  integer, parameter, public :: dp=f_double  !< density-type precision

  ! Timing categories
  integer, public, save :: TCAT_PSOLV_COMPUT=TIMING_UNINITIALIZED
  integer, public, save :: TCAT_PSOLV_COMMUN=TIMING_UNINITIALIZED
  integer, public, save :: TCAT_PSOLV_KERNEL=TIMING_UNINITIALIZED

  public :: PS_initialize_timing_categories

contains

  !> Switch on the timing categories for the Poisson Solver
  !! should be called if the time_profiling module has to be used for profiling the routines
  subroutine PS_initialize_timing_categories()
    use time_profiling, only: f_timing_category_group,f_timing_category
    use wrapper_mpi, only: comm => tgrp_mpi_name, mpi_initialize_timing_categories
    use wrapper_linalg, only: linalg_initialize_timing_categories
    implicit none
    character(len=*), parameter :: pscpt='PS Computation'

    call mpi_initialize_timing_categories()

    call linalg_initialize_timing_categories()
    !group of Poisson Solver operations, separate category
    call f_timing_category_group(pscpt,&
         'Computations of Poisson Solver operations')

  !define the timing categories
  call f_timing_category('PSolver Computation',pscpt,&
       '3D SG_FFT and related operations',&
       TCAT_PSOLV_COMPUT)
  call f_timing_category('PSolver Kernel Creation',pscpt,&
       'ISF operations and creation of the kernel',&
       TCAT_PSOLV_KERNEL)
  call f_timing_category('PSolver Communication',comm,&
       'MPI_ALLTOALL and MPI_ALLGATHERV',&
       TCAT_PSOLV_COMMUN)

  end subroutine PS_initialize_timing_categories

end module PSbase
